import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../configuration/config.service';


@Injectable()
export class AuthService {

    constructor(public httpClient: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    signUp(user: any[]) {
        return this.httpClient.post(this.apiUrl + '/auth/register', user).toPromise()
    }

    signIn(user: any[]): Promise<any> {
        return this.httpClient.post(this.apiUrl + '/auth/login', user).toPromise()
    }

}