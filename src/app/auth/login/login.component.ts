import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

import { AuthService } from '../auth.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  signinForm: FormGroup;

  constructor(private authService: AuthService, public router: Router, private toastrService: NbToastrService) { }
  config: ToasterConfig;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  ngOnInit() {
    this.signinForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50)]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(30)])
    });
  }

  login() {
    console.log(this.signinForm.value);
    this.authService.signIn(this.signinForm.value).then(res => {
      console.log('user Token ' + res.token);

      if (res.auth == true) {
        sessionStorage.setItem('userName', res.userName);
        sessionStorage.setItem('userEmail', res.email);
        sessionStorage.setItem('token', res.token);
        this.showToast('success', 'User Logged in', 'Welcome '+ res.userName);
        this.router.navigate(['/pages/dashboard']);
      } else {
        this.showToast('danger', 'Error', res.message);
      }
    })
    .catch(err => {
      this.showToast('danger', 'Err', err.message);
    })
  }

  createAccount() {
    this.router.navigate(['/signup']);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
