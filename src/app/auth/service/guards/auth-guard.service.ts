import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ConfigService } from '../../../configuration/config.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    public serviceConfig: ConfigService,
    public router: Router
  ) { }

  async canActivate():Promise<boolean>{
    let authStatus = await this.serviceConfig.isAuthenticated();
    if(!authStatus){
      this.router.navigateByUrl("/login")
      return false;
    }
    else{
      return true;
    }

  }
}
