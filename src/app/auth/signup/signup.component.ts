import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { Router } from '@angular/router'

import { AuthService } from '../auth.service';

@Component({
  selector: 'ngx-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user: any[]
  signupForm: FormGroup;

  constructor(private authService: AuthService, private router: Router, private toastrService: NbToastrService) { }
  config: ToasterConfig;

  destroyByClick = true;
  duration = 10000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  ngOnInit() {

    this.signupForm = new FormGroup({
      'firstName': new FormControl(null, [Validators.required, Validators.maxLength(30), Validators.pattern(".*\\S.*[a-zA-z ]")]),
      'lastName': new FormControl(null, [Validators.required, Validators.maxLength(30), Validators.pattern(".*\\S.*[a-zA-z ]")]),
      'userName': new FormControl(null, [Validators.required, Validators.maxLength(30), Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]),
      'about': new FormControl(null, [Validators.maxLength(80), Validators.pattern(".*\\S.*[a-zA-z ]")]),
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(30)])
    });
  }

  onSingup() {
    console.log(this.signupForm);
    this.authService.signUp(this.signupForm.value)
      .then(res => {
        this.showToast('success', 'User Registerd', res + 'Registerd!');
        this.router.navigate(['/login']);
      })
      .catch(err => {
        this.showToast('danger', 'Err', err.message);
      })

  }

  login() {
    this.router.navigate(['/login']);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    // this.index += 1;
    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
