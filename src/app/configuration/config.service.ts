import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';


@Injectable()
export class ConfigService {

    constructor(private httpClient: HttpClient) { }

    getBaseUrl() {
        // return 'http://localhost:5000/zenox';
        return 'http://139.59.15.67:5000/zenox';
    }

    async isAuthenticated(): Promise<Boolean>{
        let token = sessionStorage.getItem('token');
        if(token){
            let valid = await this.isTokenValid(token);
            return valid;

        }
        else{
            return false;
        }
    }

   private isTokenValid(token): Promise<Boolean>{
        return new Promise((resolve, reject)=>{
           let head =  this.getHeader();
            return this.httpClient.post(this.getBaseUrl() + '/auth/validatetoken', {}, { headers: head })
            .toPromise().then(result=>{
                let isvalidToken = result['validToken'];
                resolve(isvalidToken);
            })

        })

    }

   private getHeader() {
        // this.headers = null;
        let token = sessionStorage.getItem('token');
        // console.log("Token",this.token);
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': token ? token : "dummy-invalid-token"
        });

        // this.headers = headers;
        return headers;
    }

}