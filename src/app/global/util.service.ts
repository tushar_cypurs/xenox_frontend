import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from '../configuration/config.service';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(
    private configService: ConfigService,
    private httpClient: HttpClient
  ) { }

  getUnits(): Promise<any>{
      let url = `${this.configService.getBaseUrl()}/util/units`
      let unitArray = []
      let tempArray: Array<any> = []
      return new Promise((resolve,reject)=>{
        this.httpClient.get(url)
        .toPromise().then(res=>{
            tempArray = <Array<any>>res;
            tempArray.forEach(val=>{
              let item = {'id':val['id'],'unitId':val['unitId'],'label':val['unitLabel']}
              unitArray.push(item);
            });

            resolve(unitArray);
        })
        .catch(err=>{
          reject(err);
        })

      })
     
    
  }
  
}
