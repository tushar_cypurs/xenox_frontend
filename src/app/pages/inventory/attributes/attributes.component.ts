import { Component, OnInit, PipeTransform } from '@angular/core';
import { InventoryService } from '../inventory.service';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbComponentStatus, NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

import { startWith, map } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
import { Observable } from 'rxjs';
import { Attributes } from '../../../util-class/attributes';



@Component({
  selector: 'ngx-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.scss'],
  providers: [DecimalPipe]
})
export class AttributesComponent implements OnInit {

  // attributes: any;
  attributesValues: any;
  isEdit;
  isEditPage: Boolean = false;
  addAttributesForm: any;
  attributesID: any;
  data: any;
  allProduct: any;
  attributeCheck: Boolean = false;
  editFormData: any = {
    "attributeValue": []
  };

  //isDeleted: Boolean = false;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  public attributesdata: any[] = [{
    value: '',
    price: ''
  }];
  productId: any;
  valuedataarray: any = [];
  aa: boolean = false;
  isEmpty: boolean = false;
  errorMessage: string;
  errorMessageValue: string;
  errorMessagePrice: string;
  checkEdit: any;


  attributes: Array<Attributes> = [];
  attributes$: Observable<Attributes[]> = new Observable();  
  filter = new FormControl('');

  // Toaster Messages
  MSG_HEAD_MESSAGE_ATTRIBUTE_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_ATTRIBUTE_SUBMIT: string = "Attribute Added Successfully";
  MSG_HEAd_MESSAGE_ATTRIBUTE_UPDATE: string = "Successfully Updated";
  MSG_BODY_MESSAGE_ATTRIBUTE_UPDATE: string = "Attribute Updated Successfully";
  MSG_HEAD_MESSAGE_ATTRIBUTE_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_ATTRIBUTE_DELETE: string = "Attribute Deleted Successfully";
  MSG_HEAD_MESSAGE_ATTRIBUTE_ACTIVATE: string = "Successfully Activated";
  MSG_BODY_MESSAGE_ATTRIBUTE_ACTIVATE: string = "Attribute Activated Successfully";
  MSG_HEAD_MESSAGE_ATTRIBUTE_VALUE_ADD: string = "Incorrect Form";
  MSG_BODY_MESSAGE_ATTRIBUTE_VALUE_ADD: string = "Please Enter Valid Value Beteeen 1-20 Alphanumeric Character";
  MSG_HEAD_MESSAGE_ATTRIBUTE_PRICE_ADD: string = "Incorrect Form";
  MSG_BODY_MESSAGE_ATTRIBUTE_PRICE_ADD: string = "Please Enter Valid Price";



  constructor(config: NgbModalConfig, private pipe: DecimalPipe, private attributeService: InventoryService, private modalService: NgbModal, private toastrService: NbToastrService) { 

    this.attributes$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text,pipe))
    );
    this.attributes$.subscribe((data)=>{
      console.log("Obs:",data);
    })

  }


  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  ngOnInit() {

    this.addAttributesForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'productId': new FormControl(null, [Validators.required]),
      'value': new FormControl(null, Validators.maxLength(20)),
      'price': new FormControl(null, Validators.maxLength(10))
    });


    this.attributeService.getProduct()
      .then(res => {
        this.allProduct = res
      })
      .catch(err => {
        console.log("err ", err);
      })


    this.attributeService.getAttributes()
      .then(res => {
        this.attributes = res;
        console.log("attributes are", this.attributes)
        if (this.attributes.length > 0) {
          this.isEmpty = true;
        }
        else {
          this.isEmpty = false;
        }
        this.filter.setValue("");
      })
      .catch(err => {
        console.log("err ", err);
      })

  }

  getattributedata() {
    this.attributeService.getAttributes()
      .then(res => {
        this.attributes = res;
        if (this.attributes.length > 0) {
          this.isEmpty = true;
        }
        else {
          this.isEmpty = false;
        }
      })
      .catch(err => {
        console.log("err ", err);
      })
  }


  open(content, data) {
    //console.log("length is-", this.attributesdata);
    if (data) {
      console.log("Edit Page", data);
      this.isEditPage = true;
      this.editFormData = data;

      this.addAttributesForm.patchValue({
        productId: data.productId,
        name: data.name, 
      });
      this.addAttributesForm.get('productId').disable();
    }

    this.modalService.open(content, { backdrop: 'static' });
  }

  close() {
    this.editFormData = {
      "attributeValue": []
    };
    this.valuedataarray = [];
    this.isEditPage = false;
    this.addAttributesForm.reset();
    this.modalService.dismissAll();
    this.addAttributesForm.get('productId').enable();
  }

  addAttributesdata() {
    this.attributeCheck = !this.attributeCheck;
  }

  addAttributes() {
    let datavalue = this.addAttributesForm.value;

    if (datavalue.value == null || datavalue.value.trim() == "") {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_ATTRIBUTE_VALUE_ADD, this.MSG_BODY_MESSAGE_ATTRIBUTE_VALUE_ADD);
      this.errorMessageValue = this.MSG_BODY_MESSAGE_ATTRIBUTE_VALUE_ADD;
      return;
    }

    if (datavalue.price == null) {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_ATTRIBUTE_PRICE_ADD, this.MSG_BODY_MESSAGE_ATTRIBUTE_PRICE_ADD);
      this.errorMessagePrice = this.MSG_BODY_MESSAGE_ATTRIBUTE_PRICE_ADD;
      return;
    }

    let attributedata = {
      "value": datavalue.value,
      "price": datavalue.price
    };

    this.valuedataarray.push(attributedata);
    console.log("Attributes values ", this.valuedataarray);

    this.addAttributesForm.patchValue({
      "value": null,
      "price": null
    });
    this.errorMessageValue = "";
    this.errorMessagePrice = "";
  }

  deleterow(i: number) {
    this.valuedataarray.splice(i, 1);
    console.log(this.valuedataarray);
  }

  deleteValue(data: any, i: number) {
    this.attributeService.deleteAttributeMapping(data)
      .then(res => {
        console.log("Mapping deleted!", res);
        this.editFormData.attributeValue.splice(i, 1);
      })
      .catch(err => {
        console.log(err);
      })
  }

  insertAttributes() {

    if (!this.isEditPage) {

      if (this.valuedataarray.length == 0) {
        this.showToast('danger', 'Unable to Add', 'Please Add Attribute Values!');
        return;
      }

      let data = this.addAttributesForm.value;
      //let datavaluerow = this.valuedataarray;
      console.log('Data to be inserted', data);
      this.attributeService.addAttribute(data).then(res => {

        console.log("Attribute added ", res);
        let attributesid = res['id'];

        console.log("attribute id-", attributesid)

        let data = {
          "attributeId": attributesid,
          "attributeValue": []
        };
        data.attributeValue = this.valuedataarray;
        //console.log("addedd", data.attributeValue);

        this.attributeService.addAttributesdatavalue(data).then(res => {
          console.log("value added ", res);
          this.addAttributesForm.reset();
          this.getattributedata();
          this.modalService.dismissAll();
          // this.showToast('success', 'Attribute Added! ', 'Attribute added ' + res['name']);
          this.showToast('success', this.MSG_HEAD_MESSAGE_ATTRIBUTE_SUBMIT, this.MSG_BODY_MESSAGE_ATTRIBUTE_SUBMIT);
        }).catch(err => {
          console.log("err ", err);
          this.showToast('danger', 'Error! ', err);
        })
      })
    }
    else {

      let data = this.addAttributesForm.value;
      data.id = this.editFormData.id;
      this.attributeService.updateProductMapping(data)
        .then(res => {
          this.showToast('success', this.MSG_HEAd_MESSAGE_ATTRIBUTE_UPDATE, this.MSG_BODY_MESSAGE_ATTRIBUTE_UPDATE);
          console.log("Attribute Mapped! ", res);

          this.modalService.dismissAll();
          let data = {
            "attributeId": this.editFormData.id,
            "attributeValue": []
          };
          data.attributeValue = this.valuedataarray;
          this.attributeService.addAttributesdatavalue(data)
            .then(res => {
              console.log("attribute updated", res);
              // this.showToast('success',this.MSG_HEAd_MESSAGE_ATTRIBUTE_UPDATE, this.MSG_BODY_MESSAGE_ATTRIBUTE_UPDATE);
              this.editFormData = {
                "attributeValue": []
              };

            })
            .catch(err => {
              console.log(err);
            })

          this.valuedataarray = [];
          this.editFormData.attributeValue = [];
          this.isEditPage = false;
          this.addAttributesForm.reset();
          this.getattributedata();
          this.modalService.dismissAll();
          this.addAttributesForm.get('productId').enable();
        })
        .catch(err => {
          console.log(err);
        })
    }
  }


  updateMapping(data) {
    let data2 = this.addAttributesForm.value;
    data.value = data2.value;
    data.price = data2.price;

    if (this.isEditPage) {

      let objIndex = this.editFormData.attributeValue.findIndex((obj => obj.id == data.id));

      this.editFormData.attributeValue[objIndex].value = data.value;
      this.editFormData.attributeValue[objIndex].price = data.price;

    } else {
      let objIndex = this.valuedataarray.attributeValue.findIndex((obj => obj.id == data.id));

      this.valuedataarray[objIndex].value = data.value;
      this.valuedataarray[objIndex].price = data.price;
    }

  }



  deleteAttribute(data: any) {

    this.attributeService.deleteAttribute(data)
      .then(res => {
        console.log("deleted ", res);
        data.isDeleted = true;
        //this.getattributedata();
        this.showToast('success', this.MSG_HEAD_MESSAGE_ATTRIBUTE_DELETE, this.MSG_BODY_MESSAGE_ATTRIBUTE_DELETE);
      })
      .catch(err => {
        console.log(err);
        this.showToast('danger', 'Error', err);
      })
  }

  activeAttribute(data: any) {
    this.attributeService.activeAttributedata(data)
      .then(res => {
        console.log("activated ", res);
        data.isDeleted = false;
        // this.getattributedata();
        this.showToast('success', this.MSG_HEAD_MESSAGE_ATTRIBUTE_ACTIVATE, this.MSG_BODY_MESSAGE_ATTRIBUTE_ACTIVATE);
      })
      .catch(err => {
        console.log(err);
        this.showToast('danger', 'Error ', err);
      });
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config
    );
  }

  search(text: any, pipe: PipeTransform): Attributes[] {
    return this.attributes.filter(attributeValue => {
      const term = text.toLowerCase(); 
      return attributeValue.name.toLowerCase().includes(term)
        || pipe.transform(attributeValue.productName).includes(term);
    });
  }



}
