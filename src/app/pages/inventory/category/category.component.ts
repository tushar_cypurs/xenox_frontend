import { Component, OnInit, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { InventoryService } from '../inventory.service';
import { Observable } from 'rxjs';
import { Category } from '../../../util-class/category';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'ngx-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [DecimalPipe]
})
export class CategoryComponent implements OnInit {

  addCategoryForm: FormGroup;
  isEdit;
  isEditPage: Boolean = false;
  editData: any;
  // categories: any;
  categoryId: any;
  data: any;
  isEmpty: boolean = false;
  category: Array<any> = [];
  category$:Observable<Category[]> = new Observable();  
  filter = new FormControl('');

  // Toaster Messages
  MSG_HEAD_MESSAGE_CATEGORY_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_CATEGORY_SUBMIT: string = "Category Added Successfully";
  MSG_HEAd_MESSAGE_CATEGORY_UPDATE: string= "Successfully Updated";
  MSG_BODY_MESSAGE_CATEGORY_UPDATE: string= "Category Updated Successfully";
  MSG_HEAD_MESSAGE_CATEGORY_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_CATEGORY_DELETE: string = "Category Deleted Successfully";
  MSG_HEAD_MESSAGE_CATEGORY_ACTIVATE: string = "Successfully Activated";
  MSG_BODY_MESSAGE_CATEGORY_ACTIVATE: string = "Category Activated Successfully";



  constructor(pipe: DecimalPipe, config: NgbModalConfig, private modalService: NgbModal, private intService: InventoryService, private toastrService: NbToastrService) {
    config.backdrop = 'static';
    config.keyboard = false;

    this.category$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text,pipe))
    );
    this.category$.subscribe((data)=>{
      console.log("Obs:",data);
    })
  }

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  ngOnInit() {
    this.addCategoryForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.maxLength(60), Validators.pattern(".*\\S.*[a-zA-z ]")])
    });

    this.intService.getCategory()
      .then(res => {
        console.log("category data", res);
        this.category = res;
        if(this.category.length > 0){
          this.isEmpty = true;
        }else{
          this.isEmpty = false;
        }
        this.filter.setValue(" ");
      })
      .catch(err => {
        console.log("err ", err);
      })
  }

  deleteCategory(data: any) {
    this.intService.deleteCategory(data)
      .then(res => {
        console.log("deleted ", res);
        this.getCategory();
        this.showToast('success', this.MSG_HEAD_MESSAGE_CATEGORY_DELETE, this.MSG_BODY_MESSAGE_CATEGORY_DELETE);
      })
      .catch(err => {
        console.log(err);
        this.showToast('danger', 'Error ', err);
      })
  }

  activeCategory(data: any) {
    this.intService.activeCategory(data)
      .then(res => {
        console.log("activated ", res);
        this.getCategory();
        this.showToast('success', this.MSG_HEAD_MESSAGE_CATEGORY_ACTIVATE, this.MSG_BODY_MESSAGE_CATEGORY_ACTIVATE);
      })
      .catch(err => {
        console.log(err);
        this.showToast('danger', 'error ', err);
      });
  }

  open(content, category) {
    if (category) {
      this.isEditPage = true
      this.addCategoryForm.patchValue({
        name: category.name
      });
      this.categoryId = category.id;


    }
    this.modalService.open(content, { backdrop: 'static' });
  }

  close(){
    this.addCategoryForm.reset();
    this.modalService.dismissAll();
  }

  getCategory() {
    this.intService.getCategory()
      .then(res => {
        this.category = res;

        if(this.category.length > 0){
          this.isEmpty = true;
        }else{
          this.isEmpty = false;
        }
      })
      .catch(err => {
        console.log("err ", err);
      })
  }

  addCategory() {
    let data = this.addCategoryForm.value;

    if (!this.isEditPage) {
      this.intService.addCategory(data)
        .then(res => {
          console.log("category added ", res);
          this.addCategoryForm.reset();
          this.getCategory();
          this.modalService.dismissAll();
          this.showToast('success', this.MSG_HEAD_MESSAGE_CATEGORY_SUBMIT, this.MSG_BODY_MESSAGE_CATEGORY_SUBMIT);
        })
        .catch(err => {
          console.log("err ", err);
          this.showToast('danger', 'Error ', err);
        })
    } else {
      this.addCategoryForm.value.id = this.categoryId;
      this.intService.updateCategory(data)
        .then(res => {
          console.log("updated ", res);
          this.addCategoryForm.reset();
          this.modalService.dismissAll();
          this.getCategory();
          this.showToast('success', this.MSG_HEAd_MESSAGE_CATEGORY_UPDATE, this.MSG_BODY_MESSAGE_CATEGORY_UPDATE);
          this.isEditPage = false;
        })
        .catch(err => {
          console.log(err);
          this.showToast('danger', 'Error ', err);
        })
    }
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): Category[] {
    return this.category.filter(material => {
      const term = text.toLowerCase();
      return material.name.toLowerCase().includes(term);
    });
  }

}
