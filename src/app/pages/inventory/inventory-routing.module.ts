import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
// import { CheckOutComponent } from './check-out/check-out.component';
// import { AddOrderComponent } from './add-order/add-order.component';
// import { ReivewOrderComponent } from './reivew-order/reivew-order.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AttributesComponent } from './attributes/attributes.component';
import { CategoryComponent } from './category/category.component';
import { ViewProductComponent } from './view-product/view-product.component';

const routes: Routes = [
    {
        path: 'add-material',
        component: ItemComponent,
    },
    {
        path: 'edit-material/:id',
        component: ItemComponent,
    },
    {
        path: 'material-list',
        component: ItemListComponent
    },
    // {
    //     path: 'check-out',
    //     component: CheckOutComponent,
    // },
    // {
    //     path: 'add-order',
    //     component: AddOrderComponent,
    // },
    // {
    //     path: 'edit-order/:id',
    //     component: AddOrderComponent,
    // },
    // {
    //     path: 'review-order/:id',
    //     component: ReivewOrderComponent,
    // },
    

    // product

    {
        path: 'product',
        component: ProductComponent,
    },
    {
        path: 'edit-product/:id',
        component: ProductComponent,
    },
    {
        path: 'product-list',
        component: ProductListComponent,
    },
    {
        path: 'view-product/:id',
        component: ViewProductComponent,
    },
    {
        path: 'category',
        component: CategoryComponent,
    },
    {
        path: 'attributes',
        component: AttributesComponent,
    },
    {
        path: 'edit-attribute/:id',
        component: AttributesComponent,
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})

export class InventoryRoutingModule {

}
