import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemComponent } from './item/item.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { NbLayoutModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InventoryService } from './inventory.service';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from '@nebular/theme';

import { ItemListComponent } from './item-list/item-list.component';

import { NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AttributesComponent } from './attributes/attributes.component';
import { CategoryComponent } from './category/category.component';
import { ViewProductComponent } from './view-product/view-product.component';


// import { AngularMultiSelectModule } from '../../../../node_modules/angular2-multiselect-dropdown';


@NgModule({
  declarations: [ItemComponent, ItemListComponent, ProductComponent, ProductListComponent, AttributesComponent, CategoryComponent, ViewProductComponent
  ],
  imports: [
    CommonModule,
    InventoryRoutingModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgbPopoverModule
    // AngularMultiSelectModule
  ],
  providers: [InventoryService],
  exports: []
})
export class InventoryModule { }
