import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { ConfigService } from '../../configuration/config.service';


@Injectable()
export class InventoryService {
    token: any
    editData: any;
    editProduct: any;
    headers: any;

    //dummy data
    newOrder: any;
    despatchOrder: any;

    private messageSource = new BehaviorSubject('please select item');
    currentItem = this.messageSource.asObservable();

    constructor(public httpClient: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    changeMessage(supplier: any) {
        this.messageSource.next(supplier)
    }

    editItem(item: any) {
        this.editData = item;
    }

    //product edit
    editProductdata(data: any) {
        this.editProduct = data;
    }
    //product edit end

    //edit product
    getEditProduct() {
        return this.editProduct;
    }
    //edit product end

    getEditData() {
        return this.editData;
    }

    getHeader() {
        this.headers = null;
        this.token = sessionStorage.getItem('token');

        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token ? this.token : "dummy-invalid-token"
        });

        this.headers = headers;
        return headers;
    }

    getImageUrl() {
        return this.apiUrl + '/util/getfile/';
    }

    //product add
    addProduct(data: any[]): Promise<any> {
        console.log(data);
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/add', data, { headers: this.headers }).toPromise()
    }
    //product add end


    addAttribute(data: any): Promise<any> {
        console.log("ADD DATA", data);
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/productMapping', data, { headers: this.headers }).toPromise()
    }


    // update product
    updateProduct(data: any): Promise<any> {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/updateProduct', data, { headers: this.headers }).toPromise()
    }
    // update product end

    // get product 
    getProduct(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/product/getProduct', { headers: this.headers }).toPromise()
    }

    getAllProduct(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/product/getAllProduct', { headers: this.headers }).toPromise()
    }

    getProductById(data: any): Promise<any> {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/getProductById', data, { headers: this.headers }).toPromise()
    }

    // get product end


    //delete product
    deleteProduct(data: any): Promise<any> {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/delete', data, { headers: this.headers }).toPromise()
    }
    //delete product end

    //active delete product
    activeProduct(data: any): Promise<any> {
        return this.httpClient.post(this.apiUrl + '/product/activeProduct', data, { headers: this.headers }).toPromise()
    }
    // active delete product end

    // add attributes
    addAttributesdata(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/multiMappingAttribute', data, { headers: this.headers }).toPromise()
    }
    // add attributes end

    //add attributes value
    addAttributesdatavalue(data: any): Promise<any> {
        console.log("attribute value-", data)
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/attributeMapping', data, { headers: this.headers }).toPromise()
    }
    // add attributes value end

    // get Attributes value
    getAttributesvaluedata(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/product/getAttributeMapping', { headers: this.headers }).toPromise()
    }
    // get Attributes value end


    // get Attributes
    getAttributes(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/product/getProductMapping', { headers: this.headers }).toPromise()
    }
    // get Attributes end


    // delete attribute
    deleteAttribute(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/deleteProductMapping', data, { headers: this.headers }).toPromise()
    }
    // delete attributes end

    //active attribute
    activeAttributedata(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/activeProductMapping', data, { headers: this.headers }).toPromise()
    }
    //active attribute end

    //Update Product Mapping

    updateProductMapping(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/updateProductMapping', data, { headers: this.headers }).toPromise()
    }

    //Delete Attribute Value
    deleteAttributeMapping(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/deleteAttributeMapping/' + data.attributeId, data.attributeId, { headers: this.headers }).toPromise()
    }

    updateAttributeMapping(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/updateAttributeMapping', data, { headers: this.headers }).toPromise()
    }

    // get Attribute by product id

    getAttributeById(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/getProductMappingById', data, { headers: this.headers }).toPromise()
    }

    getAttributeValueById(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/product/getAttributeMappingById', data, { headers: this.headers }).toPromise()
    }

    addItem(item: any[]): Promise<any> {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/add', item, { headers: this.headers })
            .toPromise()
    }

    mapItem(data: any): Promise<any> {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/mapItemsToSupplier', data, { headers: this.headers }).toPromise()
    }

    getItem(): Promise<any> {

        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/allItem', { headers: this.headers }).toPromise()
    }

    getInventory(): Promise<any> {

        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/all', { headers: this.headers }).toPromise()
    }

    getSuppliers(): Promise<any> {

        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/supplier/get', { headers: this.headers }).toPromise()
    }

    updateItem(data: any): Promise<any> {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/update', data, { headers: this.headers }).toPromise()
    }

    deleteItem(data: any): Promise<any> {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/delete', data, { headers: this.headers }).toPromise()
    }

    activeItem(data: any): Promise<any> {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/activate', data, { headers: this.headers }).toPromise()
    }

    uploadItemImage(data: any) {
        return this.httpClient.post(this.apiUrl + '/util/uploadImage', data).toPromise()
    }

    getImages(data: any) {
        return this.httpClient.get(this.apiUrl + '/util/getfile/' + data).toPromise()
    }


    // Orders api

    addOrderList(data: any) {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/add-order', data, { headers: this.headers }).toPromise()
    }

    updateOrder(data: any) {

        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/update-order', data, { headers: this.headers }).toPromise()
    }

    getOrderList(orderId: any) {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/getOrderById/' + orderId, { headers: this.headers }).toPromise()
    }

    despatchOrders(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/dispatchedOrder', data, { headers: this.headers }).toPromise()
    }

    mapOrderItem(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/mapOrderItem', data, { headers: this.headers }).toPromise()
    }

    updateMapOrderItem(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/item/updateMapOrderItem', data, { headers: this.headers }).toPromise()
    }

    getOrderItem(orderId: any) {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/getOrderItem/' + orderId, { headers: this.headers }).toPromise()
    }

    getDespatchOrders() {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/get-order-data', { headers: this.headers }).toPromise()

    }

    // services for categories

    addCategory(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/category/add', data, { headers: this.headers }).toPromise()
    }

    updateCategory(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/category/update', data, { headers: this.headers }).toPromise()
    }

    getCategory(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/category/get', { headers: this.headers }).toPromise()
    }

    getUndeleteCategory() {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/category/getCategory', { headers: this.headers }).toPromise()
    }

    deleteCategory(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/category/delete', data, { headers: this.headers }).toPromise()
    }

    activeCategory(data: any) {
        this.getHeader();
        return this.httpClient.post(this.apiUrl + '/category/active', data, { headers: this.headers }).toPromise()
    }


}