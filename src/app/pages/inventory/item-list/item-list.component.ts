import { Component, OnInit, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { InventoryService } from '../inventory.service';
import { Router } from '@angular/router';

import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { Materials } from '../../../util-class/material';
import { Observable , of, pipe } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'ngx-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss'],
  providers: [DecimalPipe, NgbModalConfig, NgbModal]
})
export class ItemListComponent implements OnInit {
  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  isEmpty: boolean;
  materials: Array<Materials> = [];

  // Toaster Messages
  MSG_HEAD_MESSAGE_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_DELETE: string = "Material Deleted Successfully";
  MSG_HEAd_MESSAGE_ACTIVATE: string= "Successfully Activated";
  MSG_BODY_MESSAGE_ACTIVATE: string= "Material Activated Successfully";

 

  materials$:Observable<Materials[]> = new Observable();
  
  filter = new FormControl('');

  constructor(config: NgbModalConfig, private intService: InventoryService, 
    private router: Router, private toastrService: NbToastrService,
    private pipe: DecimalPipe
    ) {

      this.materials$ = this.filter.valueChanges.pipe(
        startWith(''),
        map(text => this.search(text,pipe))
      );
      this.materials$.subscribe((data)=>{
        console.log("Obs:",data);
      })

  }

  ngOnInit() {

    this.intService.getItem().then(res => {
      this.materials = <Array<Materials>>res.data;
      // this.materials$ = of(this.materials);
      this.filter.setValue(" ")
      console.log(this.materials);
      this.search(' ',this.pipe);
      if (res.data.length > 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
    }).catch(err => {
      console.log('error ',err);
    })
  }

  editITem(data: any) {
    console.log("Edit Data ", data);
    this.intService.editItem(data);
    this.router.navigate(['/pages/inventory/edit-material', data.id]);
  }

  deleteItem(item: any) {
    this.intService.deleteItem(item)
      .then(res => {
        item.isDeleted = true;
        this.showToast('success', this.MSG_HEAD_MESSAGE_DELETE, this.MSG_BODY_MESSAGE_DELETE);
      })
      .catch(err => {
        this.showToast('danger', 'Unable to delete', err.message);
      })
  }

  activateItem(item: any) {
    this.intService.activeItem(item)
      .then(res => {
        item.isDeleted = false;
        this.showToast('success', this.MSG_HEAd_MESSAGE_ACTIVATE, this.MSG_BODY_MESSAGE_ACTIVATE);
      })
      .catch(err => {
        this.showToast('danger', 'Unable to active', err.message);
      })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): Materials[] {
    return this.materials.filter(material => {
      const term = text.toLowerCase();
      return material.itemName.toLowerCase().includes(term)
          // || pipe.transform(country.area).includes(term)
          // || pipe.transform(country.population).includes(term);
    });
  }


}