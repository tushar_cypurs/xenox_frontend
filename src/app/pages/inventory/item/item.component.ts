import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { UtilService } from '../../../global/util.service';
import { InventoryService } from '../inventory.service';


@Component({
  selector: 'ngx-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  addItemForm: FormGroup;
  suppliers: any[];
  imgUrl: any = 'Choose file...';
  supplierId: any;
  editData: any;
  image: any;
  unitLabel: any[] = [
    {
      'unitId': 1,
      'label': 'KG'
    },
    {
      'unitId': 2,
      'label': 'GM'
    },
    {
      'unitId': 3,
      'label': 'LBS'
    }
  ];
  img: any;

  constructor(public inventorySer: InventoryService,
    public router: Router, private activatedRoute: ActivatedRoute,
    private toastrService: NbToastrService, private serviceUtil: UtilService) { }

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  unit: any;
  countryValue: any;
  countryName: any;
  isEdit;
  isEditPage: Boolean = false;


  // Toaster Messages
  MSG_HEAD_MESSAGE_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_SUBMIT: string = "Material Added Successfully";
  MSG_HEAd_MESSAGE_UPDATED: string= "Successfully Updated";
  MSG_BODY_MESSAGE_UPDATED: string= "Material Updated Successfully";

  ngOnInit() {

    this.serviceUtil.getUnits().then(res => {
      console.log('Item received', res);
      this.unitLabel = res || this.unitLabel;
    })

    this.addItemForm = new FormGroup({
      'itemId': new FormControl(null),
      'itemName': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'itemImage': new FormControl(null),
      'itemDescription': new FormControl(null, [Validators.required, Validators.maxLength(250),  this.noWhitespaceValidator]),
      'quantity': new FormControl(null, [Validators.required, Validators.maxLength(7),  Validators.pattern("^[0-9]*$")]),
      'price': new FormControl(null, [Validators.required, Validators.pattern("^[1-9][0-9]*$"),
      Validators.maxLength(7)]),
      'unitId': new FormControl(null),
      'unitLabel': new FormControl(null, Validators.required),
      'supplierId': new FormControl(null)
    });


    this.activatedRoute.params.subscribe(param => {
      this.isEdit = param['id']
      if (this.isEdit) {

        this.editData = this.inventorySer.getEditData();

        if (this.editData) {
          this.isEditPage = true;
          this.imgUrl = this.editData.itemImage;

          if (this.imgUrl == '' || this.imgUrl == null) {
            this.imgUrl = 'Choose file...';
          }

          this.addItemForm.patchValue({
            itemName: this.editData.itemName,
            // itemImage: this.editData.itemImage,
            itemDescription: this.editData.itemDescription,
            quantity: this.editData.quantity,
            price: this.editData.price,
            unitId: this.editData.unitId,
            unitLabel: this.editData.unitLabel,
            supplierId: this.editData.supplierId
          });

        }
      }
    })

    // this.addItemForm.
    this.inventorySer.getSuppliers().then(res => {
      this.suppliers = res.data;

    }).catch(err => {
      console.log('err ' + err);
    })
  }


  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  imageUrl(image: any) {
    this.imgUrl = image.target.files['0'].name;
    this.img = image.target.files['0'];
  }


  getItemId(event: any) {

  }


  addItem() {
    if (this.imgUrl) {
      this.addItemForm.value.itemImage = this.imgUrl;
      if (this.addItemForm.value.itemImage == "Choose file...") {
        // this.addItemForm.value.itemImage = 'defaultInt.png';
      } else {

        let formData = new FormData();
        formData.append('image_file', this.img);

        this.inventorySer.uploadItemImage(formData)
          .then(res => {
            console.log(res);

            this.addItemForm.value.itemImage = res['file-name'];

            this.addItemForm.value.unitId = 1;

            if (this.isEditPage == false) {

              this.inventorySer.addItem(this.addItemForm.value).then(res => {
                let mapData = {
                  'itemId': res.data.id.toString(),
                  'supplierId': res.data.supplierId
                }

                if (res.status == 200) {
                  console.log("updated data is", res.data.itemName)
                  this.showToast('success', this.MSG_HEAD_MESSAGE_SUBMIT, this.MSG_BODY_MESSAGE_SUBMIT );

                  this.inventorySer.mapItem(mapData).then(res => {
                    // this.showToast('success', 'Item Mapped', 'Item Mapped into Supplier');
                  }).catch(err => {
                    this.showToast('danger', 'Error', err.message);
                  });
                  this.router.navigate(['/pages/inventory/material-list']);
                }

              }).catch(err => {

                this.showToast('danger', 'Error', err.message);
              });

            } else {

              this.addItemForm.value.unitId = 1;
              this.addItemForm.value.itemId = this.editData.id;
              this.inventorySer.updateItem(this.addItemForm.value)
                .then(res => {
                  console.log("updated data is", res)
                  this.showToast('success', this.MSG_HEAd_MESSAGE_UPDATED, this.MSG_HEAd_MESSAGE_UPDATED );
                  this.router.navigate(['/pages/inventory/material-list']);
                })
                .catch(err => {
                  this.showToast('danger', 'Error', err.message);
                })

            }


          })
          .catch(err => {
            console.log(err);
          })

      }

      this.addItemForm.value.unitId = 1;

      if (this.isEditPage == false) {

        this.inventorySer.addItem(this.addItemForm.value).then(res => {
          let mapData = {
            'itemId': res.data.id.toString(),
            'supplierId': res.data.supplierId
          }

          if (res.status == 200) {
            console.log("updated data is", res.data.itemName)
            this.showToast('success', this.MSG_HEAD_MESSAGE_SUBMIT, this.MSG_BODY_MESSAGE_UPDATED );

            this.inventorySer.mapItem(mapData).then(res => {
              // this.showToast('success', 'Item Mapped', 'Item Mapped into Supplier');
            }).catch(err => {
              this.showToast('danger', 'Error', err.message);
            });
            this.router.navigate(['/pages/inventory/material-list']);
          }

        }).catch(err => {

          this.showToast('danger', 'Error', err.message);
        });

      } else {

        this.addItemForm.value.unitId = 1;
        this.addItemForm.value.itemId = this.editData.id;
        this.inventorySer.updateItem(this.addItemForm.value)
          .then(res => {
            console.log("updated data is", res)
            this.showToast('success', this.MSG_HEAd_MESSAGE_UPDATED, this.MSG_BODY_MESSAGE_UPDATED );
            this.router.navigate(['/pages/inventory/material-list']);
          })
          .catch(err => {
            this.showToast('danger', 'Error', err.message);
          })

      }

    }

  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}


