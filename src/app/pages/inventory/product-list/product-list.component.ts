import { Component, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InventoryService } from '../inventory.service';
import { Router } from '@angular/router';
//import { NbToastrService, NbComponentStatus } from '@nebular/theme';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { Observable } from 'rxjs';
import { Products } from '../../../util-class/products';
import { startWith, map } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'ngx-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers:[DecimalPipe]
})
export class ProductListComponent implements OnInit {
  modalService: any;
  //productdata: any;
  productdata: Array<Products> = [];
  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  isEmpty: boolean;
  productdata$:Observable<Products[]> = new Observable();
  filter = new FormControl('');

  // Toaster Messages
  MSG_HEAD_MESSAGE_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_DELETE: string = "Product Deleted Successfully";
  MSG_HEAd_MESSAGE_ACTIVATE: string= "Successfully Activated";
  MSG_BODY_MESSAGE_ACTIVATE: string= "Product Activated Successfully";

  constructor(config: NgbModalConfig, private productServices: InventoryService,
     private router: Router, private toastrService: NbToastrService,
     private pipe: DecimalPipe
     ) {
    this.productdata$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text,pipe))
    );
   }

  ngOnInit() {

    this.productServices.getAllProduct()
      .then(res => {
        console.log("alldata", res);
        this.productdata = res;
        if (this.productdata.length > 0) {
          this.isEmpty = true;
        } else {
          this.isEmpty = false;
        }
        this.filter.setValue(" ");
      }).catch(err => {
        console.log('error ' + err);
      })
  }

  viewProduct(data: any){
    console.log("view product data",data);
    this.router.navigate(["pages/inventory/view-product", data.id]);
  }

  deleteProduct(product: any) {
    console.log("delete product:", product);
    this.productServices.deleteProduct(product)
      .then(res => {
        product.isDeleted = true;
        this.showToast('success', this.MSG_HEAD_MESSAGE_DELETE, this.MSG_BODY_MESSAGE_DELETE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
  }

  activateProduct(product: any) {
    this.productServices.activeProduct(product)
      .then(res => {
        product.isDeleted = false;
       
        this.showToast('success', this.MSG_HEAd_MESSAGE_ACTIVATE, this.MSG_BODY_MESSAGE_ACTIVATE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
  }



  editProduct(data: any) {
    console.log("edit data is", data);
    this.productServices.editProductdata(data);
    this.router.navigate(['/pages/inventory/edit-product', data.id]);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): Products[] {
    return this.productdata.filter(material => {
      const term = text.toLowerCase().trim();
      return material.productName.toLowerCase().includes(term)
          || pipe.transform(material.id).includes(term)
          // || pipe.transform(material.sku_id).includes(term);
    });
  }

}
