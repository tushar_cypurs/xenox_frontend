import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { InventoryService } from '../inventory.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NbToastrService, NbComponentStatus, NbGlobalPosition, NbGlobalPhysicalPosition } from '@nebular/theme';
import { UtilService } from '../../../global/util.service';
import { ToasterConfig } from 'angular2-toaster';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class ProductComponent implements OnInit {

  addProductForm: FormGroup;
  addAttributesForm: FormGroup;
  suppliers: any[];
  imgUrl: any = 'Choose file...';
  supplierId: any;
  editData: any;
  image: any;
  unit: any;
  countryValue: any;
  countryName: any;
  isEdit;
  isEditPage: Boolean = false;
  rows: FormArray;
  itemForm: FormGroup;
  attributeCheck: Boolean = false;
  attributesList: any = [];

  // Attribute mapping
  attributesValues: any;
  editFormData: any = {
    "attributeValue": []
  };
  valuedataarray: any = [];
  attributeFormValues: any;

  // for modal confirmation
  isConfirmationModal: boolean = false;

  unitLabel: any[] = [
    {
      'unitId': 1,
      'label': 'KG'
    },
    {
      'unitId': 2,
      'label': 'GM'
    },
    {
      'unitId': 3,
      'label': 'LBS'
    }
  ];

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  Categories: any;
  isEditPopUp: boolean;
  // attributesValues: any = {
  //   'attributeValue': []
  // };


  disabled = false;
  ShowFilter = false;
  limitSelection = false;
  cities: any = [];
  selectedItems: any = [];
  dropdownSettings: any = {};

  selectcatArray: any = [];
  getCatdata: any;
  selectcatData: any = [];
  img: string | Blob;
  errorMessageValue: any;
  errorMessagePrice: any;

  // Toaster Messages
  MSG_HEAD_MESSAGE_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_SUBMIT: string = "Product Added Successfully";
  MSG_HEAd_MESSAGE_UPDATE: string= "Successfully Updated";
  MSG_BODY_MESSAGE_UPDATE: string= "Product Updated Successfully";
  MSG_HEAD_MESSAGE_CHECK_ATTRIBUTE_VALUE: string = "Incorrect Form";
  MSG_BODY_MESSAGE_CHECK_ATTRIBUTE_VALUE: string = "Please Enter Valid Attributes Between 1-20 Alphanumeric Character";
  MSG_HEAD_MESSAGE_CHECK_PRICE_VALUE: string = "Incorrect Form";
  MSG_BODY_MESSAGE_CHECK_PRICE_VALUE: string = "Please Enter Valid Price";



  constructor(public inventorySer: InventoryService,
    public router: Router, private activatedRoute: ActivatedRoute,
    private toastrService: NbToastrService, private serviceUtil: UtilService,
    private modalService: NgbModal, private fb: FormBuilder) { }

  getCat(getdata: any) {
    let data = {
      "categoryId": getdata.id,
      "name": getdata.name
    }
    this.selectcatData.push(data);
    console.log("selected data ", this.selectcatData);
  }

  ngOnInit() {

    this.serviceUtil.getUnits().then(res => {
      console.log('Item received', res);
      this.unitLabel = res || this.unitLabel;
    })




    this.inventorySer.getUndeleteCategory()
      .then(res => {
        this.Categories = res;
      })
      .catch(err => {
        console.log("err ", err);
      })



    this.addProductForm = new FormGroup({
      'sku_id': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'productName': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'productImage': new FormControl(null),
      'productDescription': new FormControl(null, [Validators.required, Validators.maxLength(250),  this.noWhitespaceValidator]),
      'quantity': new FormControl(null, [Validators.required, Validators.pattern("^[0-9]*$"),
      Validators.maxLength(7)]),
      'price': new FormControl(null, [Validators.required, Validators.pattern("^[1-9][0-9]*$"),
      Validators.maxLength(7)]),
      'unit_id': new FormControl(null, [Validators.required]),
      'categoryId': new FormControl(null),
      // 'attributeName': new FormControl(null),
      // 'attributeValue': new FormControl(null),
      // 'attributePrice': new FormControl(null),
    });



    this.activatedRoute.params.subscribe(param => {
      this.isEdit = param['id']
      if (this.isEdit) {

        this.editData = this.inventorySer.getEditProduct();

        if (this.editData) {
          this.isEditPage = true;
          this.imgUrl = this.editData.productImage;

          if (this.imgUrl == '' || this.imgUrl == null) {
            this.imgUrl = 'Choose file...';
          }

          this.addProductForm.patchValue({
            sku_id: this.editData.sku_id,
            productName: this.editData.productName,
            // itemImage: this.editData.itemImage,
            productDescription: this.editData.productDescription,
            quantity: this.editData.quantity,
            price: this.editData.price,
            category: this.editData.category,
            unit_id: this.editData.unit_id,
            categoryId: this.editData.categoryId,
          });

          let data = {
            'productId': this.isEdit
          }

          this.inventorySer.getAttributeById(data)
            .then(res => {
              console.log(res);
              this.attributesList = res;
              let attributeData = res;
            })
            .catch(err => {
              console.log(err);
            })

        }
        else {

          this.addProductForm.value.unitId = 1;
          this.addProductForm.value.itemId = this.editData.id;
          this.addProductForm.value.category = this.selectcatData;
          this.inventorySer.updateProduct(this.addProductForm.value)
            .then(res => {
              this.showToast('success', this.MSG_HEAD_MESSAGE_SUBMIT, this.MSG_BODY_MESSAGE_SUBMIT);
              this.router.navigate(['/pages/inventory/product-list']);
            })
            .catch(err => {
              this.showToast('danger', 'Error', err.message);
            })

        }

      }
    })


    // Attribute add form

    this.addAttributesForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.maxLength(60), Validators.pattern("[a-zA-Z ]*"), this.noWhitespaceValidator]),
      'value': new FormControl(null, [Validators.maxLength(20)]),
      'price': new FormControl(null, [Validators.pattern("^[0-9]*$"), Validators.maxLength(10)])
    });

  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  imageUrl(image: any) {
    this.imgUrl = image.target.files['0'].name;

    this.img = image.target.files['0'];
  }

  addAttributesdata() {
    this.attributeCheck = !this.attributeCheck;
  }

  addProduct() {
    if (this.imgUrl) {

      let data = this.addProductForm.value;

      // this.addProductForm.value.productImage = this.imgUrl;
      if (this.addProductForm.value.productImage == "Choose file...") {
        // this.addProductForm.value.productImage = 'defaultInt.png';
      } else {

        let formData = new FormData();
        formData.append('image_file', this.img);

        this.inventorySer.uploadItemImage(formData)
          .then(res => {
            console.log(res);

            this.addProductForm.value.productImage = res['file-name'];


            if (this.isEditPage == false) {

              this.addProductForm.value.category = this.selectcatData;
              //console.log("save data is=", this.addProductForm.value);

              this.inventorySer.addProduct(this.addProductForm.value).then(res => {

                this.showToast('success', this.MSG_HEAD_MESSAGE_SUBMIT, this.MSG_BODY_MESSAGE_SUBMIT);
                this.router.navigate(['/pages/inventory/product-list']);

                let productId = res['id'];

                let mapData = {
                  'productId': productId,
                  'attributes': this.attributesList
                };

                this.inventorySer.addAttributesdata(mapData)
                  .then(res => {
                    console.log("attribute Mapped!", res);

                  })
                  .catch(err => {
                    console.log(err);
                  })

              }).catch(err => {

                this.showToast('danger', 'Error', err.message);
              });

            }

            else {

              this.addProductForm.value.id = this.editData.id;

              console.log(this.addProductForm.value);
              this.inventorySer.updateProduct(this.addProductForm.value)
                .then(res => {
                  this.showToast('success', this.MSG_HEAd_MESSAGE_UPDATE, this.MSG_BODY_MESSAGE_UPDATE);

                  if (this.isConfirmationModal) {
                    this.router.navigate(['/pages/inventory/edit-attribute', this.isEdit]);
                    this.modalService.dismissAll();
                  } else {
                    this.router.navigate(['/pages/inventory/product-list']);
                  }

                })
                .catch(err => {
                  this.showToast('danger', 'Error', err.message);
                })

            }



          })
          .catch(err => {
            console.log(err);
          })

      }


      if (this.isEditPage == false) {

        this.addProductForm.value.category = this.selectcatData;
        //console.log("save data is=", this.addProductForm.value);

        this.inventorySer.addProduct(this.addProductForm.value).then(res => {

          this.showToast('success', this.MSG_HEAD_MESSAGE_SUBMIT, this.MSG_BODY_MESSAGE_SUBMIT);
          this.router.navigate(['/pages/inventory/product-list']);

          let productId = res['id'];

          let mapData = {
            'productId': productId,
            'attributes': this.attributesList
          };

          this.inventorySer.addAttributesdata(mapData)
            .then(res => {
              console.log("attribute Mapped!", res);

            })
            .catch(err => {
              console.log(err);
            })

        }).catch(err => {

          this.showToast('danger', 'Error', err.message);
        });

      }

      else {

        this.addProductForm.value.id = this.editData.id;

        console.log(this.addProductForm.value);
        this.inventorySer.updateProduct(this.addProductForm.value)
          .then(res => {
            this.showToast('success', this.MSG_HEAd_MESSAGE_UPDATE, this.MSG_BODY_MESSAGE_UPDATE);

            if (this.isConfirmationModal) {
              this.router.navigate(['/pages/inventory/edit-attribute', this.isEdit]);
              this.modalService.dismissAll();
            } else {
              this.router.navigate(['/pages/inventory/product-list']);
            }

          })
          .catch(err => {
            this.showToast('danger', 'Error', err.message);
          })
      }
    }

  }

  addAttributes() {
    let datavalue = this.addAttributesForm.value

    if (datavalue.value == null || datavalue.value.trim() == "") {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_CHECK_ATTRIBUTE_VALUE, this.MSG_BODY_MESSAGE_CHECK_ATTRIBUTE_VALUE);
      this.errorMessageValue = this.MSG_BODY_MESSAGE_CHECK_ATTRIBUTE_VALUE;
      return;
    }

    if (datavalue.price == null) {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_CHECK_PRICE_VALUE, this.MSG_BODY_MESSAGE_CHECK_PRICE_VALUE);
      this.errorMessagePrice = this.MSG_BODY_MESSAGE_CHECK_PRICE_VALUE;
      return;
    }

    let attributedata = {
      "value": datavalue.value,
      "price": datavalue.price
    };

    this.valuedataarray.push(attributedata);
    console.log("Attributes values ", this.valuedataarray);

    this.addAttributesForm.patchValue({
      "value": null,
      "price": null
    });

    this.errorMessagePrice = "";
    this.errorMessageValue = "";
  }

  addAttributePopup(data: any) {
    let attribute = {
      'name': this.addAttributesForm.value.name,
      'attributeValue': []
    }

    attribute.attributeValue = this.valuedataarray;
    this.attributesList.push(attribute);


    // attribute.attributeValue = null;
    this.valuedataarray = [];
    this.addAttributesForm.reset();
    this.modalService.dismissAll();

  }

  deleterow(i: number) {
    this.valuedataarray.splice(i, 1);
    // console.log(this.valuedataarray);
  }

  removeAttribute(data: any) {
    // this.attributesValues.attributeValue.splice(data);
    const index: number = this.attributesValues.attributeValue.indexOf(data);
    if (index !== -1) {
      this.attributesValues.attributeValue.splice(index, 1);
    }
    this.attributesValues.attributeValue;
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  deleteAttribute(data: any, index: number) {
    this.attributesList.splice(index, 1);
  }


  // Attribute confimation popup

  navigateToAttribute(confirmation) {
    if (this.addProductForm.touched == true) {
      this.modalService.open(confirmation, { backdrop: 'static' });
    } else {
      this.isEditPage = false;
      this.router.navigate(['/pages/inventory/edit-attribute', this.isEdit]);
      this.modalService.dismissAll();
    }
  }

  noConfirm() {
    this.isEditPage = false;
    this.router.navigate(['/pages/inventory/edit-attribute', this.isEdit]);
    this.modalService.dismissAll();
  }

  yesConfirm() {
    this.isConfirmationModal = true;
    this.addProduct()

  }

  // Attribute add models
  open(content, data) {
    if (data) {
      this.isEditPopUp = true;
      this.editFormData = data;
    }

    this.modalService.open(content, { backdrop: 'static' });
  }

  // navigateToAttribute() {
  //   this.isEditPage = false;
  //   this.router.navigate(['/pages/inventory/attributes', this.isEdit]);

  // }


  close() {
    this.editFormData = {
      "attributeValue": []
    };
    this.valuedataarray = [];
    this.isEditPopUp = false;
    this.addAttributesForm.reset();
    this.modalService.dismissAll();
  }

  insertAttributes() {
    // if(this.valuedataarray.length>0){
    //   this.showToast('danger', 'Can not added', 'Please add attribute Value !');
    //   return;
    // }
    this.attributeFormValues = this.addAttributesForm.value;
    this.addAttributesForm.reset();
    this.modalService.dismissAll();
  }


}
