import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../inventory.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {

  showdata: any;
  viewData: any;
  idView: any;
  imgurl: any;
  errUrl: string;
  isEmpty: boolean;
  catEmpty: boolean;
  categoryData: any;
  attributesData: any;

  constructor(private productservice: InventoryService, private activatedRoute: ActivatedRoute, private _location: Location, private router: Router) { }

  ngOnInit() {

    //console.log(this.viewData);

    this.activatedRoute.params.subscribe(param => {
      this.idView = param['id'];
      let data = {
        'productId': this.idView
      }
      console.log(this.idView);


      this.productservice.getProductById(data)
        .then(res => {
          console.log("data by id is", res);
          this.showdata = res;
          
          if (this.showdata.attributes.length > 0) {
            this.isEmpty = true;            
          } else {
            this.isEmpty = false;
          }

          // if(this.showdata.category.length > 0) {
          //   this.catEmpty = true;
          // } else {
          //   this.catEmpty = false;
          // }


          let url = this.productservice.getImageUrl();

          this.imgurl = url + res['productImage'];

          this.errUrl = url + 'defaultInt.png';


        }).catch(err => {
          console.log("err", err);
        })


    })


  }

  setDefaultPic() {
    this.imgurl = this.errUrl;
  }

  backClicked() {
    //this.show();
    this._location.back();
  }b

  editProduct(data: any) {
    console.log("edit data is", data);
    this.productservice.editProductdata(data);
    this.router.navigate(['/pages/inventory/edit-product', data.id]);
  }

}
