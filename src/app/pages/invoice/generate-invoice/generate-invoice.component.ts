import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'ngx-generate-invoice',
  templateUrl: './generate-invoice.component.html',
  styleUrls: ['./generate-invoice.component.scss']
})
export class GenerateInvoiceComponent implements OnInit {
  GenerateInvoice: FormGroup;
  rowdata: any = [];

  constructor() { }

  ngOnInit() {
    this.GenerateInvoice = new FormGroup({
      'item': new FormControl(null, [Validators.required]),
      'quantity': new FormControl(null, [Validators.required]),
      'price': new FormControl(null, [Validators.required]),
    })
  }

  addrow(){
    let new_row = this.GenerateInvoice.value;
    let total = this.GenerateInvoice.value.quantity*this.GenerateInvoice.value.price
    //console.log("total is", total);
    //this.rowdata.push(new_row);
    console.log(this.rowdata);
    let dt = Object.assign(new_row, total)
    console.log(dt)
    this.rowdata.push(dt);
    this.GenerateInvoice.reset();
  }

}
