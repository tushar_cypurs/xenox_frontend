import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateInvoiceComponent } from './generate-invoice/generate-invoice.component';
import { InvoiceService } from './invoice.service';
import { InvoiceRoutingModule } from './invoice-routing.module';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
     GenerateInvoiceComponent
  ],
  imports: [
    CommonModule,
     InvoiceRoutingModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ 
     InvoiceService
   ]
})
export class InvoiceModule { }
