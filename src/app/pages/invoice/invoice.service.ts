import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ConfigService } from "../../configuration/config.service";

@Injectable()
export class InvoiceService {
    token: any;
    headers: any;


    constructor(public http: HttpClient, public baseUrl: ConfigService) { }


    getHeader() {
        this.token = sessionStorage.getItem('token');
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token
        });
        this.headers = headers;
    }

   
}