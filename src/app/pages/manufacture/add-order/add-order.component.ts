import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl, FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService, COSMIC_THEME } from '@nebular/theme';
import { ManufactureService } from '../manufacture.service';



@Component({
  selector: 'ngx-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss']
})
export class AddOrderComponent implements OnInit {

  orderName: any;
  orderDate: any;
  source: any;
  destination: any;
  isData: any = false;
  orderData: any;
  orderId: any;
  updateItem: any = [];
  addItem: any = [];

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;


  orderForm: FormGroup;
  orderQuantity: any;
  isEditPage: Boolean = false;
  orderItems: any;

  inventoryList: any;
  orderList = [];
  userName: any;


  constructor(private intService: ManufactureService, private router: Router, private toastrService: NbToastrService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.userName = sessionStorage.getItem('userName');
    //console.log("data id is", this.userName)

    this.intService.getInventory()
      .then(res => {
        console.log('get data ', res);
        this.inventoryList = res.data;
        if (this.inventoryList.length !== 0) {
          this.isData = true
        } else {
          this.isData = false
        }
      })
      .catch(err => {
        console.log('Err ', err)
      })


    this.activatedRoute.params.subscribe(param => {
      this.orderId = param['id'];
      if (this.orderId) {
        this.isEditPage = true;
      }

      if (this.isEditPage) {

        this.intService.getOrderList(this.orderId)
          .then(result => {
            this.orderData = result[0];
            this.orderName = this.orderData.orderName;
            this.source = this.orderData.source;
            this.destination = this.orderData.destination;

          })
          .catch(err => {
            console.log(err);
          })

        this.intService.getOrderItem(this.orderId)
          .then(res => {
            this.orderItems = res;


            this.orderItems.forEach((item, index) => {
              //console.log(item);
              let id = parseInt(item.itemId)
              let objIndex = this.inventoryList.findIndex((obj => obj.id == id));

              this.inventoryList[objIndex].orderQuantity = item.quantity;
              this.inventoryList[objIndex].orderId = item.orderId;
              this.inventoryList[objIndex].orderItemId = item.id;
              this.inventoryList[objIndex].itemId = item.itemId;
              this.inventoryList[objIndex].isDisable = true;
            });

            this.orderItems.forEach((item, index) => {

              let id = parseInt(item.itemId)
              let findIndex = this.inventoryList.findIndex((obj => obj.id == id));

              this.updateItem.push(this.inventoryList[findIndex]);

            })

          })
          .catch(err => {
            console.log(err);
          })
      } else {

      }

    })

    // this.orderForm = new FormGroup({
    //   "orderName": new FormControl("", [Validators.required]),
    //   "source": new FormControl("", [Validators.required]),
    //   "destination": new FormControl("", [Validators.required])
    // });

  }


  addInventory(data: any) {

    if (data.errorMessage == "Please enter valid quentity" || data.orderQuantity <= 0) {
      this.showToast('danger', 'Unable to Add Inventory', 'Please enter valid quantity!');
      data.errorMessage = "Please enter valid quentity";
      return;
    } else {
      data.isDisable = true;
      this.orderList.push(data);
      if (this.isEditPage) {
        this.addItem.push(data);
      }
      console.log('  data ', this.orderList);
      this.showToast('success', 'Inventory Added', data.itemName + ' added in cart! ');
    }
  }

  reviewOrder() {

    if (this.isEditPage) {

      console.log('update order data ', this.orderList);

      this.orderDate = new Date();
      let data = {
        "orderId": this.orderId,
        "orderName": this.orderName,
        "source": this.source,
        "destination": this.destination,
        "orderDate": this.orderDate,
        "orderUser": "user",
        "orderItem": []
      };

      data.orderItem = this.orderList;
      this.intService.updateOrder(data)
        .then(res => {
          console.log("Updated order ", res);

          this.router.navigate(['/pages/manufacture/review-order', this.orderId]);

          if (this.updateItem.length !== 0) {
            data.orderItem = this.updateItem;

            this.intService.updateMapOrderItem(data)
              .then(res => {
                // console.log('data mapped ', res);
                this.updateItem = '';
              })
              .catch(err => {
                console.log(err);
              })

          }

          if (this.addItem.length !== 0) {
            data.orderItem = this.addItem;

            this.intService.mapOrderItem(data)
              .then(res => {
                // console.log('data mapped ', res);
                this.addItem = '';
              })
              .catch(err => {
                console.log(err);
              })

          }

        })
        .catch(err => {
          console.log(err);
        })


    } else {

      if (this.orderList.length < 1) {
        this.showToast('danger', 'Unable to submit Order', 'Please add inventory to cart!');
      } else {
        // console.log('order data ', this.orderList);

        this.orderDate = new Date();
        let data = {
          "orderId": "",
          "orderName": this.orderName,
          "source": this.source,
          "destination": this.destination,
          "orderDate": this.orderDate,
          "createdBy": this.userName,
          "orderUser": "user",
          "orderItem": []
        };

        data.orderItem = this.orderList;
        

        this.intService.addOrderList(data)
          .then(res => {
            console.log('Order Added!', res);

            let order = res;
            this.showToast('success', 'Order submitted', 'order submited!');
            this.router.navigate(['/pages/manufacture/review-order', order['id']]);

            data.orderId = order['id'];

            this.intService.mapOrderItem(data)
              .then(res => {
                console.log('data mapped ', res);
              })
              .catch(err => {
                console.log(err);
              })
          })
          .catch(err => {
            console.log('Unable to order', err);
          })
      }

    }

  }

  updateOrder(data) {

    if (data.errorMessage == "Please enter valid quentity" || data.orderQuantity <= 0) {
      this.showToast('danger', 'Unable to Update Inventory', 'Please enter valid quantity!');
      data.errorMessage = "Please enter valid quentity";
      return;
    } else {
      if (this.isEditPage) {

        this.orderList = this.orderItems;
        let objIndex = this.inventoryList.findIndex((obj => obj.id == data.id));
        this.orderList[objIndex].orderQuantity = data.orderQuantity;

      } else {
        let objIndex = this.inventoryList.findIndex((obj => obj.id == data.id));
        this.orderList[objIndex].orderQuantity = data.orderQuantity;
      }


      this.showToast('success', 'Inventory updated', data.itemName + ' updated quantity in cart! ');
      // console.log('updated qty ', this.orderList);
    }
  }


  varifyQty(event: any, data: any) {
    // console.log('current qty ', event.target.value)
    // console.log('inventory data', data)
    let currentQty = parseInt(event.target.value);
    if (currentQty > data.quantity) {
      data.errorMessage = "Please enter valid quentity";
    } else {
      data.errorMessage = '';
    }
  }

  // page = 1;
  // pageSize = 4;
  // collectionSize = COUNTRIES.length;

  // get countries(): Country[] {
  //   return COUNTRIES
  //     .map((country, i) => ({ id: i + 1, ...country }))
  //     .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  // }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
