import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockTransferComponent } from './stock-transfer/stock-transfer.component';
import { StockReturnComponent } from './stock-return/stock-return.component';
import { ReturnhistoryListComponent } from './returnhistory-list/returnhistory-list.component';
import { AddOrderComponent } from './add-order/add-order.component';
import { ReviewOrderComponent } from './review-order/review-order.component';
import { ViewStockReturnComponent } from './view-stock-return/view-stock-return.component';

// import { HistoryComponent } from './history/history.component';

const routes: Routes = [
    {
        path: 'stock-transfer',
        component: StockTransferComponent,
    },
    {
        path: 'stock-return',
        component: StockReturnComponent,
    },
    {
        path: 'edit-history/:id',
        component: StockReturnComponent,
    },
    {
        path: 'stock-return-list',
        component: ReturnhistoryListComponent,
    },
    {
        path: 'view-stock-return/:id',
        component: ViewStockReturnComponent,
    },
    {
        path: 'add-order',
        component: AddOrderComponent,
    },
    {
        path: 'edit-order/:id',
        component: AddOrderComponent,
    },
    {
        path: 'review-order/:id',
        component: ReviewOrderComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})

export class ManufactureRoutingModule {

}
