import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockTransferComponent } from './stock-transfer/stock-transfer.component';
import { StockReturnComponent } from './stock-return/stock-return.component';
import { ManufactureRoutingModule } from './manufacture-routing.module';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from '@nebular/theme';


import { NbLayoutModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ManufactureService } from './manufacture.service';
import { ReturnhistoryListComponent } from './returnhistory-list/returnhistory-list.component';
import { ReviewOrderComponent } from './review-order/review-order.component';
import { AddOrderComponent } from './add-order/add-order.component';
import { ViewStockReturnComponent } from './view-stock-return/view-stock-return.component';

@NgModule({
  declarations: [StockTransferComponent, StockReturnComponent, ReturnhistoryListComponent, ReviewOrderComponent, AddOrderComponent, ViewStockReturnComponent],
  imports: [
    CommonModule,
    ManufactureRoutingModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbLayoutModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  providers: [ManufactureService]
})
export class ManufactureModule { }
