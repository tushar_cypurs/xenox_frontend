import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ConfigService } from "../../configuration/config.service";

@Injectable()
export class ManufactureService {

    token: any;
    headers: any;
    editData: any;
    addStockReturnData: any = [];

    constructor(public http: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    getHeader() {
        this.token = sessionStorage.getItem('token');
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token
        });
        this.headers = headers;
    }

    // get item  
    getallItem(): Promise<any> {
        this.getHeader();
        return this.http.get(this.apiUrl + '/item/allItem', { headers: this.headers }).toPromise()
    }

    // get return history data  
    getallData(): Promise<any> {
        this.getHeader();
        return this.http.get(this.apiUrl + '/item/allItem', { headers: this.headers }).toPromise()
    }



    editHistoryData(data: any) {
        this.editData = data;
    }


    // stock transfer

    getInventory(): Promise<any> {

        this.getHeader();
        return this.http.get(this.apiUrl + '/item/all', { headers: this.headers }).toPromise()
    }

    getOrderList(orderId: any) {
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/getOrderById/' + orderId, { headers: this.headers }).toPromise()
    }

    despatchOrders(data: any) {
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/dispatchedOrder', data, { headers: this.headers }).toPromise()
    }

    mapOrderItem(data: any) {
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/mapOrderItem', data, { headers: this.headers }).toPromise()
    }

    updateMapOrderItem(data: any) {
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/updateMapOrderItem', data, { headers: this.headers }).toPromise()
    }

    getOrderItem(orderId: any) {
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/getOrderItem/' + orderId, { headers: this.headers }).toPromise()
    }

    getDespatchOrders() {
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/get-order-data', { headers: this.headers }).toPromise()

    }

    updateOrder(data: any) {

        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/update-order', data, { headers: this.headers }).toPromise()
    }

    addOrderList(data: any) {

        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/add-order', data, { headers: this.headers }).toPromise()
    }

    getStockTransferById(data: any) {
        console.log("id is-", data)
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/getStockTransferById/' + data, { headers: this.headers }).toPromise()

    }



    // stock return
    addaddStockReturn(data: any) {
        // this.addStockReturnData.push(data);
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/stockReturn', data, { headers: this.headers }).toPromise()
    }

    getStockReturn(): Promise<any> {
        // console.log(this.addStockReturnData);
        // return this.addStockReturnData;
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/getStockReturn', { headers: this.headers }).toPromise()
    }

    getRecordById(data: any): Promise<any> {
        console.log("view data form service", data)
        this.getHeader();
        return this.http.get(this.apiUrl + '/stock/getStockReturnById/' + data.productId, { headers: this.headers }).toPromise()
    }

    // get return history delete data  
    deleteHistoryData(data: any): Promise<any> {
        //console.log("data", data);
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/deleteStockReturn/'+data.id, data, { headers: this.headers }).toPromise()
    }

    // get return history activate data  
    activeHistoryData(data: any): Promise<any> {
        this.getHeader();
        return this.http.post(this.apiUrl + '/stock/activeStockReturn/'+data.id, data, { headers: this.headers }).toPromise()
    }
}