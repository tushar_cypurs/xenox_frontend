import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnhistoryListComponent } from './returnhistory-list.component';

describe('ReturnhistoryListComponent', () => {
  let component: ReturnhistoryListComponent;
  let fixture: ComponentFixture<ReturnhistoryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReturnhistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnhistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
