import { Component, OnInit, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ManufactureService } from '../manufacture.service';
import { Router } from '@angular/router';

import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { ManufactureReturnStockList } from '../../../util-class/manufactureReturnStockList';
import { DecimalPipe } from '@angular/common';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'ngx-returnhistory-list',
  templateUrl: './returnhistory-list.component.html',
  styleUrls: ['./returnhistory-list.component.scss'],
  providers: [DecimalPipe]
})
export class ReturnhistoryListComponent implements OnInit {

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  isEmpty: boolean = false;
  // history: any;
  isDeleted: boolean = false;

  //filter = new FormControl('');

  history: Array<any> = [];
  history$:Observable<ManufactureReturnStockList[]> = new Observable();  
  filter = new FormControl('');  


  //Taoster Messages

  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE: string = "Stock Return Deleted Successfully";
  MSG_HEAd_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE: string= "Successfully Activated";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE: string = "Stock Return Activated Successfully";
  
  

  constructor(pipe: DecimalPipe, config: NgbModalConfig, private manufacture: ManufactureService, private router: Router, private toastrService: NbToastrService) { 
    this.history$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text,pipe))
    );
    this.history$.subscribe((data)=>{
      console.log("Obs:",data);
    })
  }

  ngOnInit() {
    this.manufacture.getStockReturn()
    .then(res=>{
      this.history = res;
      console.log(this.history);
      if (this.history.length > 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
      this.filter.setValue("");
    }).catch(err=>{
      console.log("err", err);
    })
    //this.history = this.manufacture.getStockReturn();    
  }

  viewData(data: any){
    console.log("view data is", data);
    this.router.navigate(['pages/manufacture/view-stock-return', data.id]);
  }

  // editHistory(data: any) {
  //   console.log("Edit Data ", data);
  //   this.manufacture.editHistoryData(data);
  //   this.router.navigate(['/pages/manufacture/edit-history', data.id]);
  // }

  deleteHistory(item: any) {
    // console.log(item)
    this.manufacture.deleteHistoryData(item)
      .then(res => {
        // console.log("responce form service=", res);
        item.isDeleted = true;
        this.showToast('success', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
    // item.isDeleted = true;
    // this.showToast('success', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_DELETE);
  }

  activateHistory(item: any) {
    this.manufacture.activeHistoryData(item)
      .then(res => {
        item.isDeleted = false;
        this.showToast('success', this.MSG_HEAd_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
    //item.isDeleted = false;
    this.showToast('success', this.MSG_HEAd_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ACTIVATE);
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): ManufactureReturnStockList[] {
    return this.history.filter(returnStock => {
      const term = text.toLowerCase();     
      return returnStock.transferId.toLowerCase().includes(term);
    });
  }


}
