import { Component, OnInit } from '@angular/core';
import { ManufactureService } from '../manufacture.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ngx-review-order',
  templateUrl: './review-order.component.html',
  styleUrls: ['./review-order.component.scss']
})
export class ReviewOrderComponent implements OnInit {

 orderId: any;
  orderData: any = [];
  orderItems: any;
  orderDetails: any = [];
  inventoryList: any;

  constructor(private intService: ManufactureService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    let sub = this.route.params.subscribe(params => {
      this.orderId = +params['id'];

    });

    this.intService.getOrderList(this.orderId)
      .then(result => {
        this.orderData = result[0];

      })
      .catch(err => {
        console.log(err);
      })

    this.intService.getInventory()
      .then(res => {
        this.inventoryList = res.data;
      })
      .catch(err => {
        console.log('Err ', err)
      })

    this.intService.getOrderItem(this.orderId)
      .then(res => {
        this.orderItems = res;

        this.orderItems.forEach((item, index) => {
   
          let id = parseInt(item.itemId)
          let findIndex = this.inventoryList.findIndex((obj => obj.id == id));
          item.itemName = this.inventoryList[findIndex].itemName;

        })

      })
      .catch(err => {
        console.log(err);
      })


  }

  despatchOrder() {
    let data = {
      'orderId': this.orderId
    }
    this.intService.despatchOrders(data)
      .then(res => {
        console.log("Order Dispatched!", res);
        this.router.navigate(['/pages/manufacture/stock-transfer']);
      })
      .catch(err => {
        console.log("Order Dispatched!");
      })

  }

}
