import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ManufactureService } from '../manufacture.service';
import { Router } from '@angular/router';

import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'ngx-stock-return',
  templateUrl: './stock-return.component.html',
  styleUrls: ['./stock-return.component.scss']
})
export class StockReturnComponent implements OnInit {

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  isEmpty: boolean;
  materials: any;

  filter = new FormControl('');

  addStockReturnForm: FormGroup;
  isEdit;
  isEditPage: Boolean = false;
  valuedataarray: any = [];
  grossAmount: number = 0;
  totalQuantity = 0;
  rawMaterials: any;
  orderData: any;
  stockTransferData: any;
  stockTransferDataArray: any;
  dataStockReturn: any;
  userName: any;
  itemId: any;

  currentQty:any;
  
  errorMessageItem: any;
  errorMessage: any;
  errorMessageStatus: any;

  //Taoster Messages

  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_SUBMIT: string = "Stock Return Added Successfully";
  MSG_HEAd_MESSAGE_MANUFACTURE_STOCK_RETURN_UPDATE: string= "Successfully Updated Successfully";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_UPDATE: string= "Stock Return Updated";
  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_MATERIAL: string = "Incorrect Form";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_MATERIAL: string = "Please Choose A Valid Raw Material";
  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_QUANTITY: string = "Incorrect Form";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_QUANTITY: string = "Please Enter A Valid Quantity ( less or equal to availbale quantity )";
  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_PRICE: string = "Incorrect Form";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_PRICE: string = "Please Enter Valid Price";
  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_STATUS = "Incorrect Form";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_STATUS: string = "Please Choose Valid Status";

  MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ORDER_DATA: string = "Incorrect Form";
  MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ORDER_DATA: string = "Please Add Purchase Material Details";
  
  

  constructor(config: NgbModalConfig, private manufactureitem: ManufactureService, private router: Router, private toastrService: NbToastrService) { }
  
  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {

    this.userName = sessionStorage.getItem('userName');

    this.addStockReturnForm = new FormGroup({
      'transferId': new FormControl(null, [Validators.required]),
      'status': new FormControl(null),
      'description': new FormControl(null, [Validators.required, Validators.maxLength(250), this.noWhitespaceValidator]),
      'itemName': new FormControl(""),
      'returnQuantity': new FormControl(""),
      'itemStatus': new FormControl(""),
      'orderPrice': new FormControl("")
    });

    this.manufactureitem.getDespatchOrders()
    .then(res=>{
      console.log("data is=", res)
      this.orderData = res;
    }).catch(err=>{
      console.log("err", err)
    })

    this.manufactureitem.getInventory()
    .then(res => {
      //console.log(res.data);
      this.rawMaterials = res.data;
    }).catch(err => {
      console.log("error", err);
    })
     

  }

  getOrder(event){
    let id = event.target.value;
    this.manufactureitem.getStockTransferById(id)
    .then(res=>{
      this.stockTransferData = res;  
      this.stockTransferDataArray = this.stockTransferData.orderItems;  
    //  console.log("all data by id", this.stockTransferData);
     this.addStockReturnForm.patchValue({
      "itemName": "",
      "itemStatus": "",
      "returnQuantity": "",
      "orderPrice" : "",
    });
    }).catch(err=>{
      console.log("err", err);
    })
  }

  getReturnData(event){
    console.log("value is-", event.target.value)
    let id = event.target.value;
    let index = this.stockTransferDataArray.findIndex((obj => obj.itemName == id));
    let data = this.stockTransferDataArray[index];
    //console.log("data is --", data);
    //console.log("qty is-", data.orderQuentity);
    this.itemId = data.id;
    //console.log("item id is-", this.itemId);
    this.addStockReturnForm.patchValue({
      "itemName": data.itemName,
      "returnQuantity": data.orderQuentity,
      "orderPrice": data.price,
    });
    this.totalQuantity = data.orderQuentity;
  }

  checkQuantity(event: any) {
    //console.log('current qty ', event.target.value)
    // console.log('inventory data', this.totalQuantity)
    this.currentQty = parseInt(event.target.value);
    if (this.currentQty > this.totalQuantity) {
      this.errorMessage = "Please enter valid quantity";
      //currentQty = "";
    } else {
      this.errorMessage = '';
    }
  }

  addmaterial() {

    let datavalue = this.addStockReturnForm.value;

    if (datavalue.itemName == "") {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_MATERIAL, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_MATERIAL);
      this.errorMessageItem = this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_MATERIAL;
      return;
    }

    else if (this.totalQuantity < this.currentQty) {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_QUANTITY, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_QUANTITY);
      this.errorMessage = this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_QUANTITY;
      return;
    }
    else if(datavalue.itemStatus == ""){
      this.showToast('danger', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_STATUS, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_STATUS);
      this.errorMessageStatus = this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ADD_STATUS;
      return;
    }
    else{

    // let datavalue = this.addStockReturnForm.value;
    console.log("add qty is-", datavalue.returnQuantity)
    let rawdata = {
      "transferId": datavalue.transferId,
      "itemName": datavalue.itemName,
      "itemId": this.itemId,
      "returnQuantity": datavalue.returnQuantity,
      "orderPrice": datavalue.orderPrice,
      "total_amount": (datavalue.returnQuantity) * (datavalue.orderPrice),
      "itemStatus": datavalue.itemStatus,
    };

    this.grossAmount = this.grossAmount + rawdata.total_amount;

    this.valuedataarray.push(rawdata);
    console.log("data array is",this.valuedataarray);

    this.addStockReturnForm.patchValue({
      "itemName": "",
      "returnQuantity": "",
      "orderPrice": "",
      "total_amount": "",
      "itemStatus": ""
    });
    this.totalQuantity = 0;
    this.errorMessage = "";
    this.errorMessageStatus = "";
    this.errorMessageItem = "";
  }
}

  deleterow(data: any, i: number) {
    this.grossAmount = this.grossAmount - data.total_amount;
    this.valuedataarray.splice(i, 1);
    console.log(this.valuedataarray);
  }


  addaddStockReturn() {
    this.dataStockReturn = this.addStockReturnForm.value;

    this.dataStockReturn.stockReturnItems = this.valuedataarray;
    this.dataStockReturn.status = "initiated";
    this.dataStockReturn.transferReturnDate = new Date();
    this.dataStockReturn.totalAmount = this.grossAmount;
    this.dataStockReturn.createdBy = this.userName;

    if(this.valuedataarray.length>0 ){
    this.manufactureitem.addaddStockReturn(this.dataStockReturn)
    .then(res=>{
      console.log("save data",res);
      this.showToast('success', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_SUBMIT, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_SUBMIT); 
      this.router.navigate(['/pages/manufacture/stock-return-list']);
    }).catch(err=>{
      console.log("err", err)
    })
    }
    else
    {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_MANUFACTURE_STOCK_RETURN_ORDER_DATA, this.MSG_BODY_MESSAGE_MANUFACTURE_STOCK_RETURN_ORDER_DATA); 
    }

  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
