import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ManufactureService } from '../manufacture.service';

@Component({
  selector: 'ngx-stock-transfer',
  templateUrl: './stock-transfer.component.html',
  styleUrls: ['./stock-transfer.component.scss']
})
export class StockTransferComponent implements OnInit {

  descpatchOrderData:any;
  isEmpty: boolean = false;

  constructor(private router: Router, private intService: ManufactureService) { }

  ngOnInit() {
    this.intService.getDespatchOrders()
      .then(res => {
        this.descpatchOrderData = res;
        if(this.descpatchOrderData.length>0){
          this.isEmpty = true;
        }
        else
        {
          this.isEmpty = false;
        }
      })
      .catch(err => {
        console.log('err on get data', err);
      })
  }

  newOrder() {
    this.router.navigate(['/pages/manufacture/add-order']);
  }

  editOrder(id: any) {
    this.router.navigate(['/pages/manufacture/edit-order', id]);
  }

  reviewOrder(id: any) {
    this.router.navigate(['/pages/manufacture/review-order', id]);
  }


}
