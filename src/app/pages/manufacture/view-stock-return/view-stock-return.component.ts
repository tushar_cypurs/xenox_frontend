import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManufactureService } from '../manufacture.service';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-view-stock-return',
  templateUrl: './view-stock-return.component.html',
  styleUrls: ['./view-stock-return.component.scss']
})
export class ViewStockReturnComponent implements OnInit {

  showdata: any;
  viewData: any;
  idView: any;
  imgurl: any;
  errUrl: string;
  isEmpty: boolean;
  catEmpty: boolean;
  categoryData: any;
  attributesData: any;
  

  constructor(private stockreturn: ManufactureService, private activatedRoute: ActivatedRoute, private _location: Location, private router: Router) { }

  ngOnInit() {

    //console.log(this.viewData);

    this.activatedRoute.params.subscribe(param => {
      this.idView = param['id'];
      console.log("id is", this.idView);
      let data = {
        'productId': this.idView
      }
      console.log(this.idView);


      this.stockreturn.getRecordById(data)
        .then(res => {
          console.log("data by id is", res);
          this.showdata = res;
          
          if (this.showdata.stockReturnItems.length > 0) {
            this.isEmpty = true;            
          } else {
            this.isEmpty = false;
          }

          // if(this.showdata.category.length > 0) {
          //   this.catEmpty = true;
          // } else {
          //   this.catEmpty = false;
          // }

        }).catch(err => {
          console.log("err", err);
        })


    })


  }

  setDefaultPic() {
    this.imgurl = this.errUrl;
  }

  backClicked() {
    //this.show();
    this._location.back();
  }

  

}
