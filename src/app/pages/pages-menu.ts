import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'DASHBOARD',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'INVENTORY MANAGEMENT',
    icon: 'cube-outline',

    children: [
      {
        title: 'Raw Materials',
        icon: 'pantone-outline',
        children: [
          {
            title: 'Add Material',
            link: '/pages/inventory/add-material'
          },
          {
            title: 'All Material',
            link: '/pages/inventory/material-list'
          },
          // {
          //   title: 'Check out',
          //   link: '/pages/inventory//check-out'
          // }
        ]
      },

      {
        title: 'Finished Product',
        icon: 'shopping-cart-outline',
        children: [
          {
            title: 'Add Product',
            link: '/pages/inventory/product'
          },
          {
            title: 'All Product',
            link: '/pages/inventory/product-list'
          },
          {
            title: 'Category',
            link: '/pages/inventory/category'
          },
          {
            title: 'Attributes',
            link: '/pages/inventory/attributes'
          }
        ]
      },

    ]
  },

  {
    title: 'SUPPLIERS MANAGEMENT',
    icon: 'people-outline',
    children: [
      {
        title: 'Add suppliers',
        link: '/pages/suppliers/add-supplier'
      },
      {
        title: 'Suppliers list',
        link: '/pages/suppliers/suppliers-list'
      },
      {
        title: 'Purchase Order',
        link: '/pages/suppliers/purchase-order'
      },
      {
        title: 'Purchase Order List',
        link: '/pages/suppliers/purchase-order-list'
      },
      {
        title: 'Stock Return',
        link: '/pages/suppliers/stock-return'
      },
      {
        title: 'View Stock Return ',
        link: '/pages/suppliers/stock-return-history'
      },
      // {
      //   title: 'Credit Memos',
      //   link: '/pages/suppliers/credit-memos'
      // }
    ]
  },


  // {
  //   title: 'INVOICE',
  //   icon: 'file-text-outline',
  //   children: [
  //     {
  //       title: 'Generate Invoice',
  //       link: '/pages/invoice/generate-invoice'
  //     }
  //     ,
  //     {
  //       title: 'Invoice list',
  //       link: '/pages/invoice/invoice-list'
  //     }
  //   ]
  // },

  {
    title: 'MANUFACTURING',
    icon: 'shake-outline',
    children: [
      {
        title: 'Stock Transfer',
        link: '/pages/manufacture/stock-transfer'
      },
      {
        title: 'Add Stock Return',
        link: '/pages/manufacture/stock-return'
      }, 
      {
        title: 'View Stock Return',
        link: '/pages/manufacture/stock-return-list'
      },    
      // {
      //   title: 'History',
      //   link: '/pages/manufacture/history'
      // },
      // {
      //   title: 'Purchase Order',
      //   link: '/pages/suppliers/purchase-order'
      // },
      // {
      //   title: 'Credit Memos',
      //   link: '/pages/suppliers/credit-memos'
      // }
    ]
  },

  // {
  //   title: 'TRANSACTION',
  //   icon: 'swap-outline',
  //   children: [

  //   ]
  // },


  // {
  //   title: 'User',
  //   icon: 'lock-outline',
  //   children: [
  //     {
  //       title: 'signin',
  //       link: '/pages/user/signin',
  //     },
  //     {
  //       title: 'Signup',
  //       link: '/pages/user/signup',
  //     }
  //   ]
  // },
  // {
  //   title: 'IoT Dashboard',
  //   icon: 'home-outline',
  //   link: '/pages/iot-dashboard',
  // },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Layout',
  //   icon: 'layout-outline',
  //   children: [
  //     {
  //       title: 'Stepper',
  //       link: '/pages/layout/stepper',
  //     },
  //     {
  //       title: 'List',
  //       link: '/pages/layout/list',
  //     },
  //     {
  //       title: 'Infinite List',
  //       link: '/pages/layout/infinite-list',
  //     },
  //     {
  //       title: 'Accordion',
  //       link: '/pages/layout/accordion',
  //     },
  //     {
  //       title: 'Tabs',
  //       pathMatch: 'prefix',
  //       link: '/pages/layout/tabs',
  //     },
  //   ],
  // },
  // {
  //   title: 'Forms',
  //   icon: 'edit-2-outline',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/pages/forms/inputs',
  //     },
  //     {
  //       title: 'Form Layouts',
  //       link: '/pages/forms/layouts',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/pages/forms/buttons',
  //     },
  //     {
  //       title: 'Datepicker',
  //       link: '/pages/forms/datepicker',
  //     },
  //   ],
  // },
  // {
  //   title: 'UI Features',
  //   icon: 'keypad-outline',
  //   link: '/pages/ui-features',
  //   children: [
  //     {
  //       title: 'Grid',
  //       link: '/pages/ui-features/grid',
  //     },
  //     {
  //       title: 'Icons',
  //       link: '/pages/ui-features/icons',
  //     },
  //     {
  //       title: 'Typography',
  //       link: '/pages/ui-features/typography',
  //     },
  //     {
  //       title: 'Animated Searches',
  //       link: '/pages/ui-features/search-fields',
  //     },
  //   ],
  // },
  // {
  //   title: 'Modal & Overlays',
  //   icon: 'browser-outline',
  //   children: [
  //     {
  //       title: 'Dialog',
  //       link: '/pages/modal-overlays/dialog',
  //     },
  //     {
  //       title: 'Window',
  //       link: '/pages/modal-overlays/window',
  //     },
  //     {
  //       title: 'Popover',
  //       link: '/pages/modal-overlays/popover',
  //     },
  //     {
  //       title: 'Toastr',
  //       link: '/pages/modal-overlays/toastr',
  //     },
  //     {
  //       title: 'Tooltip',
  //       link: '/pages/modal-overlays/tooltip',
  //     },
  //   ],
  // },
  // {
  //   title: 'Extra Components',
  //   icon: 'message-circle-outline',
  //   children: [
  //     {
  //       title: 'Calendar',
  //       link: '/pages/extra-components/calendar',
  //     },
  //     {
  //       title: 'Progress Bar',
  //       link: '/pages/extra-components/progress-bar',
  //     },
  //     {
  //       title: 'Spinner',
  //       link: '/pages/extra-components/spinner',
  //     },
  //     {
  //       title: 'Alert',
  //       link: '/pages/extra-components/alert',
  //     },
  //     {
  //       title: 'Calendar Kit',
  //       link: '/pages/extra-components/calendar-kit',
  //     },
  //     {
  //       title: 'Chat',
  //       link: '/pages/extra-components/chat',
  //     },
  //   ],
  // },
  // {
  //   title: 'Maps',
  //   icon: 'map-outline',
  //   children: [
  //     {
  //       title: 'Google Maps',
  //       link: '/pages/maps/gmaps',
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       link: '/pages/maps/leaflet',
  //     },
  //     {
  //       title: 'Bubble Maps',
  //       link: '/pages/maps/bubble',
  //     },
  //     {
  //       title: 'Search Maps',
  //       link: '/pages/maps/searchmap',
  //     },
  //   ],
  // },
  // {
  //   title: 'Charts',
  //   icon: 'pie-chart-outline',
  //   children: [
  //     {
  //       title: 'Echarts',
  //       link: '/pages/charts/echarts',
  //     },
  //     {
  //       title: 'Charts.js',
  //       link: '/pages/charts/chartjs',
  //     },
  //     {
  //       title: 'D3',
  //       link: '/pages/charts/d3',
  //     },
  //   ],
  // },
  // {
  //   title: 'Editors',
  //   icon: 'text-outline',
  //   children: [
  //     {
  //       title: 'TinyMCE',
  //       link: '/pages/editors/tinymce',
  //     },
  //     {
  //       title: 'CKEditor',
  //       link: '/pages/editors/ckeditor',
  //     },
  //   ],
  // },
  // {
  //   title: 'Tables & Data',
  //   icon: 'grid-outline',
  //   children: [
  //     {
  //       title: 'Smart Table',
  //       link: '/pages/tables/smart-table',
  //     },
  //     {
  //       title: 'Tree Grid',
  //       link: '/pages/tables/tree-grid',
  //     },
  //   ],
  // },
  // {
  //   title: 'Miscellaneous',
  //   icon: 'shuffle-2-outline',
  //   children: [
  //     {
  //       title: '404',
  //       link: '/pages/miscellaneous/404',
  //     },
  //   ],
  // },
  // {
  //   title: 'Auth',
  //   icon: 'lock-outline',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
