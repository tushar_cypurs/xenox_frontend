import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpplierComponent } from './add-supplier.component';

describe('AddSpplierComponent', () => {
  let component: AddSpplierComponent;
  let fixture: ComponentFixture<AddSpplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
