import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';

import { SuppliersService } from '../suppliers.service';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'ngx-add-spplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.scss']
})
export class AddSpplierComponent implements OnInit {
  addSupplierForm: FormGroup;
  imgUrl: any = 'Choose file...';
  items: any;
  currentItem: any;
  isEdit;
  editData;
  isEditPage: Boolean = false;
  img: string | Blob;

  constructor(private supService: SuppliersService, private cd: ChangeDetectorRef, private route: Router, private activatedRoute: ActivatedRoute, private toastrService: NbToastrService) { }
  config: ToasterConfig;

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  // Toaster Messages
  MSG_HEAD_MESSAGE_ADD_SUPPLIER_SUBMIT: string = "Successfully Added";
  MSG_BODY_MESSAGE_ADD_SUPPLIER_SUBMIT: string = "Supplier Added Successfully";
  MSG_HEAd_MESSAGE_ADD_SUPPLIER_UPDATE: string= "Successfully Updated";
  MSG_BODY_MESSAGE_ADD_SUPPLIER_UPDATE: string= "Supplier Updated Successfully";
    

  ngOnInit() {

    this.addSupplierForm = new FormGroup({
      'supplierId': new FormControl(null),
      'name': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'contactNumber': new FormControl(null, [Validators.required, Validators.pattern("[1-9]{1}[0-9]{9}"), Validators.maxLength(10)]),
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50)]),
      'image': new FormControl(null),
      'itemId': new FormControl(''),
      'companyId': new FormControl(null, [Validators.required, Validators.maxLength(50),  this.noWhitespaceValidator]),
      'description': new FormControl(null, [Validators.required, Validators.maxLength(250),  this.noWhitespaceValidator]),
      'address': new FormControl(null, [Validators.required, Validators.maxLength(250),  this.noWhitespaceValidator])
    });


    this.activatedRoute.params.subscribe(param => {
      this.isEdit = param['id']
      if (this.isEdit) {

        this.editData = this.supService.getSupp();
        console.log('editData', this.editData)

        if (this.editData) {
          this.isEditPage = true;
          this.imgUrl = this.editData.image;

          if (this.imgUrl == '' || this.imgUrl == null) {
            this.imgUrl = 'Choose file...';
          }

          this.addSupplierForm.patchValue({
            name: this.editData.name,
            contactNumber: this.editData.contactNumber,
            email: this.editData.email,
            itemId: this.editData.itemId,
            companyId: this.editData.companyId,
            description: this.editData.description,
            address: this.editData.address
          });

        }
      }
    })


    this.supService.getItem().then(res => {
      this.items = res.data;
    }).catch(err => {
      console.log('Err ', err);
    });

  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  imageUrl(url: any) {
    this.imgUrl = url.target.files['0'].name;
    this.img = url.target.files['0'];
  }

  addSubpplier() {
    console.log(this.addSupplierForm.value);
    this.currentItem = this.addSupplierForm.value.itemId;
    this.addSupplierForm.value.image = this.imgUrl;
    if (this.addSupplierForm.value.image == "Choose file...") {
      // this.addSupplierForm.value.image = 'default.png';
    } else {
      let formData = new FormData();
      formData.append('image_file', this.img);

      this.supService.uploadSupplierImage(formData)
        .then(res => {
          console.log(res);

          this.addSupplierForm.value.image = res['file-name'];


          if (this.isEditPage == false) {

            this.supService.add(this.addSupplierForm.value).then(res => {
              this.showToast('success', this.MSG_HEAD_MESSAGE_ADD_SUPPLIER_SUBMIT, this.MSG_BODY_MESSAGE_ADD_SUPPLIER_SUBMIT);
              if (res.status == 200) {
                let data = {
                  'supplierId': res.data.id,
                  'itemId': this.currentItem
                }

                this.supService.mapSupplier(data)
                  .then(res => {
                    // this.showToast('success', 'Supplier Mapped', 'Supplier Mapped ');
                  })
                  .catch(err => {
                    this.showToast('danger', 'Error', err);
                  })

                this.route.navigate(['/pages/suppliers/suppliers-list']);
              }

            }).catch(err => {
              this.showToast('danger', 'Error', err);
            });

          } else {
            this.addSupplierForm.value.supplierId = this.editData.id;
            this.supService.updateSupplier(this.addSupplierForm.value)
              .then(res => {
                this.showToast('success', this.MSG_HEAd_MESSAGE_ADD_SUPPLIER_UPDATE, this.MSG_BODY_MESSAGE_ADD_SUPPLIER_UPDATE);
                this.route.navigate(['/pages/suppliers/suppliers-list']);
              })
              .catch(err => {
                this.showToast('danger', 'Error', err);
              })
          }


        })
        .catch(err => {
          console.log(err);
        })
    }


    if (this.isEditPage == false) {

      this.supService.add(this.addSupplierForm.value).then(res => {
        this.showToast('success', this.MSG_HEAD_MESSAGE_ADD_SUPPLIER_SUBMIT, this.MSG_BODY_MESSAGE_ADD_SUPPLIER_SUBMIT);
        if (res.status == 200) {
          let data = {
            'supplierId': res.data.id,
            'itemId': this.currentItem
          }

          this.supService.mapSupplier(data)
            .then(res => {
              // this.showToast('success', 'Supplier Mapped', 'Supplier Mapped ');
            })
            .catch(err => {
              this.showToast('danger', 'Error', err.message);
            })

          this.route.navigate(['/pages/suppliers/suppliers-list']);
        }

      }).catch(err => {
        this.showToast('danger', 'Error', err.message);
      });

    } else {
      this.addSupplierForm.value.supplierId = this.editData.id;
      this.supService.updateSupplier(this.addSupplierForm.value)
        .then(res => {
          this.showToast('success', this.MSG_HEAd_MESSAGE_ADD_SUPPLIER_UPDATE, this.MSG_BODY_MESSAGE_ADD_SUPPLIER_UPDATE);
          this.route.navigate(['/pages/suppliers/suppliers-list']);
        })
        .catch(err => {
          this.showToast('danger', 'Error', err.message);
        })
    }

  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    // this.index += 1;
    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
