import { Component, OnInit, Input } from '@angular/core';
import { SuppliersService } from '../suppliers.service';
import { Router } from '@angular/router';

import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'ngx-detail-layout',
  templateUrl: './detail-layout.component.html',
  styleUrls: ['./detail-layout.component.scss']
})
export class DetailLayoutComponent implements OnInit {
  supplier: any;

  constructor(private supService: SuppliersService, private route: Router, private toastrService: NbToastrService) { }

  destroyByClick = true;
  duration = 10000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  ngOnInit() {
    this.supService.currentMessage.subscribe(supplier => {
      this.supplier = supplier;
      

    });

  }

  editSupplier(data: any) {
    this.route.navigate(['/pages/suppliers/edit-supplier', data.id]);
    this.supService.singleSupplier(data);

  }

  deleteSupp(supplierId: any) {
    this.supService.deleteSupplier(supplierId)
      .then(res => {
        this.supplier.isDeleted = true;
        this.showToast('success', 'Supplier Deleted', 'Supplier Deleted ');
      })
      .catch(err => {
        this.showToast('danger', 'Unable to Delete', err.message);
      })
  }

  activeSupplier(supplierId: any) {
    this.supService.activeSupplier(supplierId)
      .then(res => {
        this.supplier.isDeleted = false;
        this.showToast('success', 'Supplier activated', 'Supplier Activated ');
      })
      .catch(err => {
        this.showToast('danger', 'Unable to activation', err.message);
      })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
