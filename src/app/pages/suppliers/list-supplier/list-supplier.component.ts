import { Component, OnInit, PipeTransform } from '@angular/core';
import { Router } from '@angular/router';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { Suppliers } from '../../../util-class/supliers';
import { SuppliersService } from '../suppliers.service';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'ngx-list-supplier',
  templateUrl: './list-supplier.component.html',
  styleUrls: ['./list-supplier.component.scss'],
  providers: [NgbModalConfig, NgbModal, DecimalPipe]
})
export class ListSupplierComponent implements OnInit {
  config: ToasterConfig;
  spplier: any;
  suppliers: Array<Suppliers> = [];
  suppliers$:Observable<Suppliers[]> = new Observable();
  filter = new FormControl('');
  data: any[];
  isEmpty: Boolean;

  // Toaster Messages
  MSG_HEAD_MESSAGE_SUPPLIER_DELETE: string = "Successfully Deleted";
  MSG_BODY_MESSAGE_SUPPLIER_DELETE: string = "Supplier Deleted Successfully";
  MSG_HEAd_MESSAGE_SUPPLIER_ACTIVATE: string= "Successfully Activated";
  MSG_BODY_MESSAGE_SUPPLIER_ACTIVATE: string= "Supplier Activated Successfully";
  

  constructor(config: NgbModalConfig, private supplierSer: SuppliersService,
     private route: Router, private toastrService: NbToastrService,
     private pipe: DecimalPipe) {
      this.suppliers$ = this.filter.valueChanges.pipe(
        startWith(''),
        map(text => this.search(text,pipe))
      );
      
      }

  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  ngOnInit() {
    this.supplierSer.currentMessage.subscribe(spplier => this.spplier = spplier)

    this.supplierSer.getSupplier().then(res => {
      console.log('Data is fetched!', res.data);
      this.suppliers = res.data;
      if (res.data.length > 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
      this.filter.setValue(" ");
    }).catch(err => {
      console.log('Error');
    })
  }

  editSupplier(data: any) {
    this.route.navigate(['/pages/suppliers/edit-supplier', data.id]);
    this.supplierSer.singleSupplier(data);
  }

  deleteSupplier(data: any) {
    this.supplierSer.deleteSupplier(data)
      .then(res => {
        //console.log("delete res", res)
        data.isDeleted = true;
        this.showToast('success', this.MSG_HEAD_MESSAGE_SUPPLIER_DELETE, this.MSG_BODY_MESSAGE_SUPPLIER_DELETE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
  }

  activateSupplier(data: any) {
    this.supplierSer.activeSupplier(data)
      .then(res => {
        data.isDeleted = false;
        this.showToast('success', this.MSG_HEAd_MESSAGE_SUPPLIER_ACTIVATE, this.MSG_BODY_MESSAGE_SUPPLIER_ACTIVATE);
      })
      .catch(err => {
        this.showToast('danger', 'Error', err.message);
      })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
      
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): Suppliers[] {
    return this.suppliers.filter(material => {
      const term = text.toLowerCase().trim();
      return material.name.toLowerCase().includes(term)
          || material.email.toLowerCase().includes(term)
          || material.companyId.toLowerCase().includes(term)
          || pipe.transform(material.contactNumber).includes(term)
    });
  }

}
