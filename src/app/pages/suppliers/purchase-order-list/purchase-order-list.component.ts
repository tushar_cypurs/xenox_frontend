import { Component, OnInit, PipeTransform } from '@angular/core';
import { SuppliersService } from '../suppliers.service';
import { Router } from '@angular/router';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService, NbComponentStatus } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { SupplierPurchaseOrderList } from '../../../util-class/supplierPurchaseOrderList';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import { startWith, map } from 'rxjs/operators';


@Component({
  selector: 'ngx-purchase-order-list',
  templateUrl: './purchase-order-list.component.html',
  styleUrls: ['./purchase-order-list.component.scss'],
  providers: [DecimalPipe]
})
export class PurchaseOrderListComponent implements OnInit {

  isEmpty: Boolean;
  purchaseorderdata: any;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  supplierName: any;
  allData: Array<any> = [];
  allData$:Observable<SupplierPurchaseOrderList[]> = new Observable();  
  filter = new FormControl('');
  

  // Toaster Messages
  MSG_HEAD_MESSAGE_PURCHASE_ORDER_DELETE: string = "Successfully Delete";
  MSG_BODY_MESSAGE_PURCHASE_ORDER_DELETE: string = "Purchase Order Deleted Successfully";
  MSG_HEAd_MESSAGE_PURCHASE_ORDER_ACTIVATE: string = "Successfully Activated";
  MSG_BODY_MESSAGE_PURCHASE_ORDER_ACTIVATE: string = "Purchase Order Activated Successfully";
  arrayData: any;
  arr1: any = [];
  arr2: any = [];
  supplierDetails: any;
  
  supplierData: Object;
  singleSupplierName: any;


  constructor(pipe: DecimalPipe, private purchaseorder: SuppliersService, private router: Router, private toastrService: NbToastrService) { 
    this.allData$ = this.filter.valueChanges.pipe(
      startWith(''),
      map(text => this.search(text,pipe))
    );
    this.allData$.subscribe((data)=>{
      console.log("Obs:",data);
    })
  }

  ngOnInit() {

    this.purchaseorder.getPurchaseData()
      .then(res => {
        this.allData = res;
        this.supplierName = res.supplierName;


        // for (let i = 0; i < this.allData.length; i++) {
        //   let value = this.allData[i];
        //   let valueData = value['purchaseDetail'];
        //   let idSupplier = valueData['supplierId'];

        //   this.purchaseorder.getSupplierName(idSupplier)
        //     .then(res => {
        //       this.supplierData = res;
        //       this.supplierName = this.supplierData
        //       this.singleSupplierName = this.supplierName.name;
        //       this.allData[i]['purchaseDetail'].supplierName = this.singleSupplierName;

        //     }).catch(err => {
        //       console.log("error", err)
        //     })
        //     this.filter.setValue("");
            
        // }

        console.log("res data", this.allData);
        if (this.allData.length > 0) {
          this.isEmpty = true;
        } else {
          this.isEmpty = false;
        }
        this.filter.setValue(" ");
      }).catch(err => {
        console.log("error", err);
      })

  }

  viewPurchaseOrder(viewData: any) {
    this.router.navigate(['/pages/suppliers/view-purchase-order-detail', viewData.id]);
    this.purchaseorder.singlePurchaseOrderData(viewData);
  }

  editpurchase(data: any) {
    this.router.navigate(['/pages/suppliers/edit-history', data.id]);
    this.purchaseorder.singlePurchaseOrderData(data);
  }

  deletepurchase(item: any) {
    item.isDeleted = true;
    this.showToast('success', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_DELETE, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_DELETE);


    // this.manufacture.deleteHistoryData(item)
    //   .then(res => {
    //     item.isDeleted = true;
    //     this.showToast('success', 'History deleted', 'History Deleted');
    //   })
    //   .catch(err => {
    //     this.showToast('danger', 'Unable to delete', err.message);
    //   })
  }

  activatepurchase(item: any) {
    item.isDeleted = false;
    this.showToast('success', this.MSG_HEAd_MESSAGE_PURCHASE_ORDER_ACTIVATE, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ACTIVATE);
    // this.manufacture.activeHistoryData(item)
    //   .then(res => {
    //     item.isDeleted = false;
    //     this.showToast('success', 'History activated', 'History activated');
    //   })
    //   .catch(err => {
    //     this.showToast('danger', 'Unable to active', err.message);
    //   })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

  search(text: string, pipe: PipeTransform): any[] {
    return this.allData.filter(order => {
      const term = text.toLowerCase().trim(); 
      // console.log("-",order['purchaseDetail']['id']);    
      return order.supplierName.toLowerCase().includes(term)
        || (order.purchaseDetail.id.toString()).includes(term)
        || (order.purchaseDetail.orderNote).includes(term)
        || (order.purchaseDetail.paymentMethod).includes(term);
    });
  }


}
