import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SuppliersService } from '../suppliers.service';
import { NbDateService } from '@nebular/theme';
import { ObjectUnsubscribedError } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService, NbComponentStatus } from '@nebular/theme';

@Component({
  selector: 'ngx-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {

  addPurchaseOrderForm: FormGroup;
  supplierName: any;
  min: Date;
  max: Date;
  valuedataarray: any = [];
  grossAmount: number = 0;
  rawMaterials: any;
  qty: any;
  itemPrice: any;
  totalQuantity = 0;
  addDataObject: any;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  errorMessageItem: any;
  errorMessageQty: any;
  currentQty: number;
  isEditPage: Boolean = false;

   // Toaster Messages
   MSG_HEAD_MESSAGE_PURCHASE_ORDER_SUBMIT: string = "Successfully Added";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_SUBMIT: string = "Purchase Order Added Successfully";
   MSG_HEAd_MESSAGE_PURCHASE_ORDER_UPDATE: string= "Successfully Updated";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_UPDATE: string= "Purchase Order Updated Successfully";
   MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_MATERIAL: string = "Incorrect Form";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_MATERIAL: string = "Please Choose A Valid Raw Material";
   MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY: string = "Incorrect Form";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY: string = "Please Enter A Valid Quantity ( less or equal to availbale quantity )";
   MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_PRICE: string = "Incorrect Form";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_PRICE: string = "Please Enter Valid Price";
   MSG_HEAD_MESSAGE_PURCHASE_ORDER_ORDER_DATA: string = "Incorrect Form";
   MSG_BODY_MESSAGE_PURCHASE_ORDER_ORDER_DATA: string = "Please Add Purchase Material Details";
  supplierId: any;
  materialId: any;
  isEdit: any;
  editData: any;

  constructor(private purchaseOrder: SuppliersService, protected dateService: NbDateService<Date>, private router: Router, private activatedRoute: ActivatedRoute, private toastrService: NbToastrService) { }

  ngOnInit() {
    this.addPurchaseOrderForm = new FormGroup({
      'supplierId': new FormControl(null, [Validators.required]),
      'estimateTime': new FormControl(null, [Validators.required]),
      // 'invoicestatus': new FormControl(null, [Validators.required]),
      'materialId': new FormControl(""),
      'materialQty': new FormControl("", [Validators.pattern("^[0-9]*$")]),
      'materialAmount': new FormControl("", [Validators.pattern("^[1-9][0-9]*$")]),
      'billingAddress': new FormControl(null, [Validators.required, this.noWhitespaceValidator]),
      'shippingAddress': new FormControl(null),
      'paymentMethod': new FormControl(null),
      'orderNote': new FormControl(null),
    })

    
    this.activatedRoute.params.subscribe(param => {
      this.isEdit = param['id']
      console.log("isEdit", this.isEdit);
      if (this.isEdit) { 

        this.editData = this.purchaseOrder.getPurchaseOrder();
        console.log("editData", this.editData)

        if (this.editData) {
          this.isEditPage = true; 
          this.addPurchaseOrderForm.patchValue({
            supplierId: this.editData.supplierId,
            estimateTime: this.editData.estimateTime,
            billingAddress: this.editData.billingAddress,
            shippingAddress: this.editData.shippingAddress,
            paymentMethod: this.editData.paymentMethod,
            orderNote: this.editData.orderNote,
          });

        }
      }
    })

    this.purchaseOrder.getSupplier()
      .then(res => {
        //this.supplierName = res.data;
        // console.log(this.supplierName);
        this.supplierId = res.data;
        console.log(this.supplierId)
      }).catch(err => {
        console.log("error", err);
      })

    this.purchaseOrder.getMaterial()
      .then(res => {
        console.log(res.data);
        this.rawMaterials = res.data;
      }).catch(err => {
        console.log("error", err);
      })
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }


  getdata(event) {
    console.log("value is", event.target.value)
    let id = event.target.value;
    let index = this.rawMaterials.findIndex((obj => obj.itemName == id));
    let data = this.rawMaterials[index];
    this.materialId = data.id;
    console.log("get fetch data", data)
    
    this.addPurchaseOrderForm.patchValue({
      "materialQty": data.quantity,
      "materialAmount": data.price,
    });

    this.totalQuantity = data.quantity;
  }

  checkQuantity(event: any) {
    console.log('current qty ', event.target.value)
    console.log('inventory data', this.totalQuantity)
    this.currentQty = parseInt(event.target.value);
    if (this.currentQty > this.totalQuantity) {
      this.errorMessageQty = "Please enter valid quantity";
      this.showToast('danger', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY);
      this.errorMessageQty = this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY;
    } else {
      this.errorMessageQty = '';
    }
  }

  addmaterial() {
    let datavalue = this.addPurchaseOrderForm.value;

    if(datavalue.materialId==""){
      this.showToast('danger', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_MATERIAL, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_MATERIAL);
      this.errorMessageItem = this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_MATERIAL
      return;
    }
    if(this.totalQuantity < this.currentQty ||  datavalue.materialQty == "") {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY);
      this.errorMessageQty = this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_QUANTITY;
      return;
    }
    if(datavalue.materialAmount == "") {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_ADD_PRICE, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_PRICE );
      this.errorMessageQty = this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ADD_PRICE;
      return;
    }


    let rawdata = {
      "materialName": datavalue.materialId,
      "materialId": this.materialId,
      "materialQty": datavalue.materialQty,
      "materialAmount": datavalue.materialAmount,
      "total_amount": (datavalue.materialQty) * (datavalue.materialAmount),
    };

    this.grossAmount = this.grossAmount + rawdata.total_amount;

    this.valuedataarray.push(rawdata);
    console.log(this.valuedataarray);

    this.addPurchaseOrderForm.patchValue({
      "materialName": "",
      "materialId": "",
      "materialQty": "",
      "materialAmount": "",
      "total_amount": "",
    });
    this.totalQuantity = 0;
    this.errorMessageItem = "";
    this.errorMessageQty = "";
  }

  deleterow(data: any, i: number) {
    this.grossAmount = this.grossAmount - data.total_amount;
    this.valuedataarray.splice(i, 1);
    console.log(this.valuedataarray);
  }

  AddToTextArea() {
    let billing = this.addPurchaseOrderForm.value;
    // console.log(billing.billing_address);
    this.addPurchaseOrderForm.patchValue({
      'shippingAddress': billing.billingAddress,
    })
  }

  addPurchaseOrder() {

    if (!this.isEditPage) 
    {
      this.addPurchaseOrderForm.value.grossAmount = this.grossAmount;
      this.addPurchaseOrderForm.value.purchaseOrderMaterials = this.valuedataarray;
      //console.log("save data is-", this.addPurchaseOrderForm.value);

      if(this.valuedataarray.length>0){
        this.purchaseOrder.addPurchaseData(this.addPurchaseOrderForm.value)
        .then(res=>{
          console.log("response data-",res);
          this.showToast('success', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_SUBMIT, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_SUBMIT);
          this.router.navigate(['pages/suppliers/purchase-order-list']);
        }).catch(err=>{
          console.log("err", err)
        })
        
      }
      else{
        this.showToast('danger', this.MSG_HEAD_MESSAGE_PURCHASE_ORDER_ORDER_DATA, this.MSG_BODY_MESSAGE_PURCHASE_ORDER_ORDER_DATA);
        return;
      }

    } else {
      console.log('update');
    }
    
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
