import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { SuppliersService } from '../suppliers.service';


@Component({
  selector: 'ngx-search-layout',
  templateUrl: './search-layout.component.html',
  styleUrls: ['./search-layout.component.scss']
})
export class SearchLayoutComponent implements OnInit {
  spplier: any;
  suppliers: any[];
  data: any[];
  isEmpty: Boolean;


  constructor(private supplierSer: SuppliersService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.supplierSer.currentMessage.subscribe(spplier => this.spplier = spplier)

    this.supplierSer.getSupplier().then(res => {
      console.log('Data is featch!');
      this.suppliers = res.data;
      if (res.data.length > 0) {
        this.isEmpty = true;
      } else {
        this.isEmpty = false;
      }
    }).catch(err => {
      console.log('Error');
    })
  }

  addNewItem(supp: any) {
    this.supplierSer.changeMessage(supp)
  }

}
