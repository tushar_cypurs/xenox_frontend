import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockReturnHistoryComponent } from './stock-return-history.component';

describe('StockReturnHistoryComponent', () => {
  let component: StockReturnHistoryComponent;
  let fixture: ComponentFixture<StockReturnHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockReturnHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockReturnHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
