import { Component, OnInit } from '@angular/core';
import { SuppliersService } from '../suppliers.service';
import { Router } from '@angular/router';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService, NbComponentStatus } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'ngx-stock-return-history',
  templateUrl: './stock-return-history.component.html',
  styleUrls: ['./stock-return-history.component.scss']
})
export class StockReturnHistoryComponent implements OnInit {
  history: any;
  isEmpty: boolean;
  isDeleted: boolean = false;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

   // Toaster Messages
   MSG_HEAD_MESSAGE_STOCK_RETURN_DELETE: string = "Successfully Delete";
   MSG_BODY_MESSAGE_STOCK_RETURN_DELETE: string = "Stock Return Deleted Successfully";
   MSG_HEAd_MESSAGE_STOCK_RETURN_ACTIVATE: string= "Successfully Activated";
   MSG_BODY_MESSAGE_STOCK_RETURN_ACTIVATE: string= "Stock Return Activated Successfully";

  constructor(private stockReturnHistory: SuppliersService, private router: Router, private toastrService: NbToastrService) { }

  ngOnInit() {
    this.history = this.stockReturnHistory.getStockReturnformData();
    console.log(this.history);
    if (this.history.length > 0) {
      this.isEmpty = true;
    } else {
      this.isEmpty = false;
    }
    // .then(res=>{
    //   this.history = res.data;
    //   console.log(this.history);
    // })
  }

  viewHistory(data: any){
    console.log("view Data ", data);
    // this.router.navigate(['/pages/suppliers/view-stock-return', data.purchaseOrderId]);
    this.router.navigate(['/pages/suppliers/view-stock-return', data.purchaseOrderId]);
  }

  editHistory(data: any) {
    console.log("Edit Data ", data.purchaseOrderId);
   // this.stockReturnHistory.editHistoryData(data);
    this.router.navigate(['/pages/suppliers/edit-history', data.purchaseOrderId]);
  }

  deleteHistory(item: any) {
    item.isDeleted = true;
    this.showToast('success', this.MSG_HEAD_MESSAGE_STOCK_RETURN_DELETE, this.MSG_BODY_MESSAGE_STOCK_RETURN_DELETE);
    // this.manufacture.deleteHistoryData(item)
    //   .then(res => {
    //     item.isDeleted = true;
    //     this.showToast('success', 'History deleted', 'History Deleted');
    //   })
    //   .catch(err => {
    //     this.showToast('danger', 'Unable to delete', err.message);
    //   })
  }

  activateHistory(item: any) {
    item.isDeleted = false;
    this.showToast('success', this.MSG_HEAD_MESSAGE_STOCK_RETURN_DELETE, this.MSG_BODY_MESSAGE_STOCK_RETURN_DELETE);
    // this.manufacture.activeHistoryData(item)
    //   .then(res => {
    //     item.isDeleted = false;
    //     this.showToast('success', 'History activated', 'History activated');
    //   })
    //   .catch(err => {
    //     this.showToast('danger', 'Unable to active', err.message);
    //   })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
