import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SuppliersService } from '../suppliers.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToasterConfig } from 'angular2-toaster';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbToastrService, NbComponentStatus } from '@nebular/theme';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-stock-return',
  templateUrl: './stock-return.component.html',
  styleUrls: ['./stock-return.component.scss']
})
export class StockReturnComponent implements OnInit {

  addStockReturnForm: FormGroup;
  valuedataarray: any = [];
  totalGrossAmount: any = [];
  grossAmount = 0;
  rawMaterials: any;
  qty: any;
  itemPrice: any;
  totalQuantity = 0;
  addDataObject: any;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;

  isEdit;
  editData: any;
  isEditPage: Boolean = false;
  errorMessageMaterial: any;
  errorMessageQty: any;
  errorMessageStatus: any;
  currentQty: number;
  
  /*
  Error message literals
   
  */
   // Toaster Messages
   MSG_HEAD_MESSAGE_STOCK_RETURN_SUBMIT: string = "Successfully Added";
   MSG_BODY_MESSAGE_STOCK_RETURN_SUBMIT: string = "Stock Return Added Successfully";
   MSG_HEAd_MESSAGE_STOCK_RETURN_UPDATE: string= "Successfully Updated";
   MSG_BODY_MESSAGE_STOCK_RETURN_UPDATE: string= "Stock Return Updated Successfully";
   MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_MATERIAL: string = "Incorrect Form";
   MSG_BODY_MESSAGE_STOCK_RETURN_ADD_MATERIAL: string = "Please Choose A Valid Raw Material";
   MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_QUANTITY: string = "Incorrect Form";
   MSG_BODY_MESSAGE_STOCK_RETURN_ADD_QUANTITY: string = "Please Enter A Valid Quantity ( less or equal to availbale quantity )";
   MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_PRICE: string = "Incorrect Form";
   MSG_BODY_MESSAGE_STOCK_RETURN_ADD_PRICE: string = "Please Enter Valid Price";
   MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_STATUS = "Incorrect Form";
   MSG_BODY_MESSAGE_STOCK_RETURN_ADD_STATUS: string = "Please Choose Valid Status";

   MSG_HEAD_MESSAGE_STOCK_RETURN_ORDER_DATA: string = "Incorrect Form";
   MSG_BODY_MESSAGE_STOCK_RETURN_ORDER_DATA: string = "Please Add Purchase Material Details";
   

  constructor(private returnOrder: SuppliersService, private router: Router, config: NgbModalConfig, private toastrService: NbToastrService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.addStockReturnForm = new FormGroup({
      "purchaseOrderId": new FormControl(null, [Validators.required]),
      "raw_material_name": new FormControl(""),
      "quantity": new FormControl("", [Validators.pattern("^[0-9]*$")]),
      "amount": new FormControl("", [Validators.pattern("^[0-9]*$")]),
      "status": new FormControl(""),
      "return_note": new FormControl(null, [Validators.required, Validators.pattern(".*\\S.*[a-zA-z ]")]),
    })

    this.activatedRoute.params.subscribe(param => {
      this.isEdit = param['id']
      console.log("edit page-", this.isEdit)


      let data = this.returnOrder.getStockReturnformData();
      this.editData = data[0];
      console.log("edit page data-", this.editData)
      if (this.isEdit) {
        this.isEditPage = true;
        console.log("this edit page-", this.isEditPage)
        this.addStockReturnForm.patchValue({
          purchaseOrderId: this.editData.purchaseOrderId,
          return_note: this.editData.return_note,
          // valuedataarray: this.editData.valuedataarray
        });
        this.grossAmount = this.editData.grossAmount;
        this.valuedataarray = this.editData.valuedataarray;

      }
    })

    this.returnOrder.getMaterial()
      .then(res => {
        console.log(res.data);
        this.rawMaterials = res.data;
      }).catch(err => {
        console.log("error", err);
      })
  }

  getdata(event) {
    console.log("value is", event.target.value)
    let id = event.target.value;
    let index = this.rawMaterials.findIndex((obj => obj.itemName == id));
    let data = this.rawMaterials[index];

    this.addStockReturnForm.patchValue({
      "quantity": data.quantity,
      "amount": data.price,
    });

    this.totalQuantity = data.quantity;
  }

  checkQuantity(event: any) {
    console.log('current qty ', event.target.value)
    console.log('inventory data', this.totalQuantity)
    this.currentQty = parseInt(event.target.value);
    if (this.currentQty > this.totalQuantity) {
      this.errorMessageQty = "Please enter valid quantity";
      this.showToast('danger',this.MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_QUANTITY, this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_QUANTITY);
      this.errorMessageQty = this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_QUANTITY;
    } else {
      this.errorMessageQty = '';
    }
  }


  addmaterial() {
    let datavalue = this.addStockReturnForm.value;

    if(datavalue.raw_material_name == ""){
      this.showToast('danger', this.MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_MATERIAL, this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_MATERIAL);
      this.errorMessageMaterial = this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_MATERIAL;
      return;
    }
    if(datavalue.quantity == "" || this.currentQty > this.totalQuantity){
      this.showToast('danger', this.MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_QUANTITY, this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_QUANTITY);
      this.errorMessageQty = this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_QUANTITY;
      return;
    }
    if(datavalue.status == ""){
      this.showToast('danger', this.MSG_HEAD_MESSAGE_STOCK_RETURN_ADD_STATUS, this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_STATUS);
      this.errorMessageStatus = this.MSG_BODY_MESSAGE_STOCK_RETURN_ADD_STATUS;
      return;
    }    

    let rawdata = {
      "raw_material_name": datavalue.raw_material_name,
      "quantity": datavalue.quantity,
      "amount": datavalue.amount,
      "status": datavalue.status,
      "total_amount": (datavalue.quantity) * (datavalue.amount),
    };

    this.grossAmount = this.grossAmount + rawdata.total_amount;

    this.valuedataarray.push(rawdata);
    console.log(this.valuedataarray);

    this.addStockReturnForm.patchValue({
      "raw_material_name": "",
      "quantity": "",
      "amount": "",
      "status": "",
      "total_amount": "",
    });
    this.totalQuantity = 0;
    this.errorMessageMaterial = "";
    this.errorMessageQty = "";
    this.errorMessageStatus = "";
  }

  

  deleterow(data: any, i: number) {
    this.grossAmount = this.grossAmount - data.total_amount;
    this.valuedataarray.splice(i, 1);
    console.log(this.valuedataarray);
  }



  addStockReturn() {
    // let d = Object.assign(this.grossAmount)
    // console.log("amount are-", d)
    this.addStockReturnForm.value.grossAmount = this.grossAmount;
    this.addStockReturnForm.value.valuedataarray = this.valuedataarray;
    //let datavalue = this.addStockReturnForm.value.valuedataarray
    //let savedata = Object.assign(this.addStockReturnForm.value, datavalue);
    console.log("save", this.addStockReturnForm.value)

    if(this.valuedataarray.length>0)
    {
      this.returnOrder.addStockReturnformData(this.addStockReturnForm.value);
      this.showToast('success', this.MSG_HEAD_MESSAGE_STOCK_RETURN_SUBMIT, this.MSG_BODY_MESSAGE_STOCK_RETURN_SUBMIT);
      this.router.navigate(['pages/suppliers/stock-return-history']);
    }
    else
    {
      this.showToast('danger', this.MSG_HEAD_MESSAGE_STOCK_RETURN_ORDER_DATA, this.MSG_BODY_MESSAGE_STOCK_RETURN_ORDER_DATA);
      return;
    }

    //this.showToast('danger', 'Unable to delete', 'History Not Deleted');
    // this.router.navigate("/pages/suppliers/stock-return");
    // .then(res=>{
    //   console.log("res is-",res);
    // }).catch(err=>{
    //   console.log("error", err)
    // })
  }

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}


