import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddSpplierComponent } from './add-supplier/add-supplier.component';
import { ListSupplierComponent } from './list-supplier/list-supplier.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { CreditMemosComponent } from './credit-memos/credit-memos.component';
import { PurchaseOrderListComponent } from './purchase-order-list/purchase-order-list.component';
import { StockReturnComponent } from './stock-return/stock-return.component';
import { StockReturnHistoryComponent } from './stock-return-history/stock-return-history.component';
import { ViewStockReturnComponent } from './view-stock-return/view-stock-return.component';
import { ViewPurchaseOrderDetailComponent } from './view-purchase-order-detail/view-purchase-order-detail.component';

const routes: Routes = [
    {
        path: 'add-supplier',
        component: AddSpplierComponent,
    },
    {
        path: 'edit-supplier/:id',
        component: AddSpplierComponent,
    },
    {
        path: 'suppliers-list',
        component: ListSupplierComponent
    },
    {
        path: 'purchase-order', 
        component: PurchaseOrderComponent
    },
    {
        path: 'edit-history/:id',
        component: PurchaseOrderComponent,
    },
    {
        path: 'view-purchase-order-detail/:id',
        component: ViewPurchaseOrderDetailComponent,
    },
    {
        path: 'purchase-order-list', 
        component: PurchaseOrderListComponent
    },
    {
        path: 'stock-return',
        component: StockReturnComponent
    },
    {
        path: 'stock-return-history',
        component: StockReturnHistoryComponent
    },
    {
        path: 'edit-history/:id',
        component: StockReturnComponent,
    },
    {
        path: 'view-stock-return/:id',
        component: ViewStockReturnComponent,
    },
    // {
    //     path: 'credit-memos',
    //     component: CreditMemosComponent
    // }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})

export class SuppliersRoutingModule {

}
