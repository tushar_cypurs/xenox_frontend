import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbLayoutModule } from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from '@nebular/theme';
import { NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { SuppliersService } from './suppliers.service';
import { AddSpplierComponent } from './add-supplier/add-supplier.component';
import { ListSupplierComponent } from './list-supplier/list-supplier.component';
import { SuppliersRoutingModule } from './suppliers-routing.module';
import { SearchLayoutComponent } from './search-layout/search-layout.component';
import { DetailLayoutComponent } from './detail-layout/detail-layout.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { CreditMemosComponent } from './credit-memos/credit-memos.component';
import { PurchaseOrderListComponent } from './purchase-order-list/purchase-order-list.component';
import { StockReturnComponent } from './stock-return/stock-return.component';
import { StockReturnHistoryComponent } from './stock-return-history/stock-return-history.component';
import { ViewStockReturnComponent } from './view-stock-return/view-stock-return.component';
import { ViewPurchaseOrderDetailComponent } from './view-purchase-order-detail/view-purchase-order-detail.component';
// import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AddSpplierComponent, ListSupplierComponent, SearchLayoutComponent, DetailLayoutComponent,
    PurchaseOrderComponent, 
    CreditMemosComponent, 
    PurchaseOrderListComponent, 
    StockReturnComponent, StockReturnHistoryComponent, ViewStockReturnComponent, ViewPurchaseOrderDetailComponent
  ],
  imports: [
    CommonModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NbLayoutModule,
    SuppliersRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgbPopoverModule
  ],
  providers: [SuppliersService]
})
export class SuppliersModule { }
