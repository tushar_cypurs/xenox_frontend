import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { ConfigService } from "../../configuration/config.service";

@Injectable()
export class SuppliersService {
    supplier: any;
    token: any;
    headers: any;

    stockReturn: any = [];
    purchaseData: any = [];

    private messageSource = new BehaviorSubject('please select supplier');
    currentMessage = this.messageSource.asObservable();
    editData: any;
    purchaseOrderData: any = [];

    constructor(public http: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    getHeader() {
        this.token = sessionStorage.getItem('token');
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token
        });
        this.headers = headers;
    }

    changeMessage(supplier: any) {
        this.messageSource.next(supplier)
    }

    add(supplier: any[]): Promise<any> {

        this.getHeader();
        return this.http.post(this.apiUrl + '/supplier/add', supplier, { headers: this.headers }).toPromise()
    }

    updateSupplier(data: any): Promise<any> {
        this.getHeader();
        return this.http.post(this.apiUrl + '/supplier/update', data, { headers: this.headers }).toPromise()

    }

    getSupplier(): Promise<any> {

        this.getHeader();
        return this.http.get(this.apiUrl + '/supplier/getAll', { headers: this.headers }).toPromise()
    }

    mapSupplier(data: any): Promise<any> {

        this.getHeader();
        return this.http.post(this.apiUrl + '/item/mapItemsToSupplier', data, { headers: this.headers }).toPromise()
    }

    getItem(): Promise<any> {
        this.getHeader();
        return this.http.get(this.apiUrl + '/item/all', { headers: this.headers }).toPromise()
    }

    deleteSupplier(data: any): Promise<any> {
        // console.log("delete data foem service", data);
        this.getHeader();
        return this.http.post(this.apiUrl + '/supplier/delete', data, { headers: this.headers }).toPromise()
    }

    activeSupplier(data: any): Promise<any> {
        this.getHeader();
        return this.http.post(this.apiUrl + '/supplier/active', data, { headers: this.headers }).toPromise()
    }

    singleSupplier(data: any) {
        this.supplier = data;
    }

    getSupp() {
        return this.supplier;
    }

    getMaterial(): Promise<any> {
        this.getHeader();
        return this.http.get(this.apiUrl + '/item/all', { headers: this.headers }).toPromise()
    }

    // Add Purchase Order
    // addPurchaseOrderData(fromdata: any, rawMaterial: any, totalAmount: any) {

    // }

    editHistoryData(data: any) {
        console.log("edit", data)
        this.editData = data;
    }

    addStockReturnformData(data: any) {
        this.stockReturn.push(data);
    }

    getStockReturnformData() {
        return this.stockReturn;
    }

    // purchase order

    addPurchaseData(data: any[]): Promise<any> {

        this.getHeader();
        return this.http.post(this.apiUrl + '/purchase/addPurchaseOrder', data, { headers: this.headers }).toPromise()
    }

    getPurchaseData(): Promise<any> {
        this.getHeader();
        return this.http.get(this.apiUrl + '/purchase/getAllPurchaseOrder', { headers: this.headers }).toPromise()
    }

    uploadSupplierImage(data: any) {
        return this.http.post(this.apiUrl + '/util/uploadImage', data).toPromise()
    }

    singlePurchaseOrderData(data: any) {
        this.purchaseOrderData = data;
        console.log("singlePurchaseOrderData", this.purchaseOrderData);
    }

    getPurchaseOrder() {
        // console.log("singlePurchaseOrderData res", this.purchaseOrderData);
        return this.purchaseOrderData;      
    }


    getSupplierName(data: any) {
        console.log("supplier id is", data);
        return this.http.get(this.apiUrl + '/supplier/get/' + data, { headers: this.headers }).toPromise()
    }

    getPurchaseOrderById(){
        return this.purchaseOrderData;
    }
    


}