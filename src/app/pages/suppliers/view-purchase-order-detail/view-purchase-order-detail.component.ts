import { Component, OnInit } from '@angular/core';
import { SuppliersService } from '../suppliers.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'ngx-view-purchase-order-detail',
  templateUrl: './view-purchase-order-detail.component.html',
  styleUrls: ['./view-purchase-order-detail.component.scss']
})
export class ViewPurchaseOrderDetailComponent implements OnInit {
  viewData: any;
  showdata: any;
  idView: any;
  isEmpty: boolean = false;

  constructor(private purchaseorder: SuppliersService, private activatedRoute: ActivatedRoute, private router: Router, private _location: Location) { }

  ngOnInit() {
      this.activatedRoute.params.subscribe(param => {
        this.idView = param['id'];
        let data = {
          'productId': this.idView
        }
        // this.purchaseorder.getPurchaseOrderById()
        // .then(res => {
        //   console.log("data by id is", res);
        //   this.showdata = res;
          
        //   if (this.showdata.attributes.length > 0) {
        //     this.isEmpty = true;            
        //   } else {
        //     this.isEmpty = false;
        //   }
        // }).catch(err => {
        //   console.log("err", err);
        // })
    })
  }

  backClicked() {
    //this.show();
    this._location.back();
  }

}
