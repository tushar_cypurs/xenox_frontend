import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStockReturnComponent } from './view-stock-return.component';

describe('ViewStockReturnComponent', () => {
  let component: ViewStockReturnComponent;
  let fixture: ComponentFixture<ViewStockReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStockReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStockReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
