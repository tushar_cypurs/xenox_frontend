import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  newOrder() {
    this.router.navigate(['/pages/transaction/add-order']);

  }

}
