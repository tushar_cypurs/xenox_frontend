import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CheckOutComponent } from './check-out/check-out.component';
import { AddOrderComponent } from './add-order/add-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';

const routes: Routes = [
    {
        path: 'transaction',
        component: CheckOutComponent,
    },
    {
        path: 'check-out',
        component: CheckOutComponent,
    },
    {
        path: 'add-order',
        component: AddOrderComponent,
    },
    {
        path: 'view-order',
        component: ViewOrderComponent,
    }

];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
})

export class TransactionRoutingModule {

}
