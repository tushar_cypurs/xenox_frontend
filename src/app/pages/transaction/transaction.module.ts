import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbLayoutModule } from '@nebular/theme';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule
} from '@nebular/theme';

import { CheckOutComponent } from './check-out/check-out.component';
import { TransactionRoutingModule } from './transaction-routing.module';
import { TransactionService } from './transaction.service';
import { AddOrderComponent } from './add-order/add-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CheckOutComponent, AddOrderComponent, ViewOrderComponent],
  imports: [
    CommonModule,
    TransactionRoutingModule,
    NbLayoutModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TransactionService]
})
export class TransactionModule { }
