import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


import { ConfigService } from '../../configuration/config.service';


@Injectable()
export class TransactionService {
    token: any
    editData: any;
    headers: any;

    constructor(public httpClient: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    getHeader() {
        this.headers = null;
        this.token = sessionStorage.getItem('token');
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token
        });

        this.headers = headers;
        return headers;
    }

    getItem(): Promise<any> {
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/item/allItem', { headers: this.headers }).toPromise()
    }


}