import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbGlobalPosition, NbGlobalPhysicalPosition, NbComponentStatus, NbToastrService } from '@nebular/theme';
import { ToasterConfig } from 'angular2-toaster';
import { Router } from '@angular/router'
import { AuthService } from '../../../auth/auth.service';
import { UserService } from '../user.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'ngx-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  updatePasswordForm: FormGroup;
  imgUploadForm: FormGroup;
  userName: any;
  imgUrl: any = 'Choose file...';
  img: any;

  config: ToasterConfig;
  destroyByClick = true;
  duration = 3000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  errUrl: string;
  data: Object;
  imgurl: any;
  userEmail: string;
  userData: any;

  // Toaster Messages
  MSG_HEAd_MESSAGE_USER_UPDATED: string= "Successfully Updated";
  MSG_HEAd_MESSAGE_USER_ERROR: string= "Incorrect Form";
  MSG_BODY_MESSAGE_USER_IMAGE_UPDATED: string= "Image Updated Successfully";
  MSG_BODY_MESSAGE_USER_DATA_UPDATED: string= "Profile Updated Successfully";
  MSG_BODY_MESSAGE_USER_DATA_PASSWORD_UPDATED: string= "Password Updated Successfully";
  MSG_BODY_MESSAGE_USER_DATA_OLD_PASSWORD_UPDATED: string= "Something Went Wrong !";

  

  constructor(private authService: AuthService,private modalService: NgbModal, private router: Router, private toastrService: NbToastrService, private user: UserService) { }
  

  ngOnInit() {

    this.userName = sessionStorage.getItem('userName');
    this.userEmail = sessionStorage.getItem('userEmail');

    this.getUserDetails(); 

    this.user.getEditData(this.userEmail)
      .then(res => {
        let data = res[0];
        this.userData = data;
        // this.errUrl =  'defaultInt.png';
        this.editProfileForm.patchValue({
          'firstName': data['firstName'],
          'lastName': data['lastName'],
          'userName': data['userName'],
          'email': data['email'],
        });
      })
      .catch(err => {
        console.log('Err ', err)
      })

    this.editProfileForm = new FormGroup({
      'firstName': new FormControl(null, [Validators.required, Validators.maxLength(30), this.noWhitespaceValidator]),
      'lastName': new FormControl(null, [Validators.required, Validators.maxLength(30), this.noWhitespaceValidator]),
      'userName': new FormControl(null, [Validators.required, Validators.maxLength(30), this.noWhitespaceValidator ]),
      // 'about': new FormControl(null, [Validators.maxLength(80) ]),
      'email': new FormControl(null, [Validators.required, Validators.email, Validators.maxLength(50), Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")])
    });

    this.updatePasswordForm = new FormGroup({
      // 'old_pass': new FormControl(null, [Validators.required, Validators.maxLength(60)]),
      'new_pass': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
      'con_pass': new FormControl(null, [Validators.required, Validators.maxLength(60), this.noWhitespaceValidator]),
    }, passwordMatchValidator);
    
    function passwordMatchValidator(g: FormGroup) {
        let pass = g.controls.new_pass.value;
        let confirmPass = g.controls.con_pass.value;
        return pass === confirmPass ? null : { notSame: true }
    }

    this.imgUploadForm = new FormGroup({
      'image': new FormControl(null, [Validators.required])
    })

     
  }

  

 

  getUserDetails(){
    this.user.getUserImage(this.userEmail)
    .then(res => {
      console.log("data image is", res);
      this.userData = res[0].pas
      let url = this.user.getImageUrl();
      this.imgurl = url + res[0]['userImage'];
      this.errUrl = url + 'default.png';
    }).catch(err => {
      console.log("err", err);
    })  
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  open(content) {  
    console.log("click-", content)
    this.modalService.open(content, { backdrop: 'static' });
  }

  close(){
    this.updatePasswordForm.reset();
    this.modalService.dismissAll();
  }

  imageUrl(image: any) {
    this.imgUrl = image.target.files['0'].name;
   // console.log("img url is", this.imgUrl)
    this.img = image.target.files['0'];
  }

  imgUpload(){   
    // this.imgUploadForm.value.email = this.userEmail;
    // this.imgUploadForm.value.userImage = this.imgUrl;

    let formData = new FormData();
    formData.append('image_file', this.img);
    this.user.uploadUserImage(formData)
    .then(res=>{
      console.log("img is", res);
      this.imgUploadForm.value.email = this.userEmail;
      this.imgUploadForm.value.userImage = res['file-name'];        
      this.user.updateUserData(this.imgUploadForm.value)
      .then(res=>{
        console.log("update response is ", res);
        this.showToast('success', this.MSG_HEAd_MESSAGE_USER_UPDATED, this.MSG_BODY_MESSAGE_USER_IMAGE_UPDATED );
        this.imgUploadForm.reset;
        this.getUserDetails();
      }).catch(err=>{
        console.log("error", err);
      })

    }).catch(err=>{
      console.log("error", err);
    })
  }

  editProfile(){
    console.log("update profile", this.editProfileForm.value);
    this.user.updateUserData(this.editProfileForm.value)
    .then(res=>{
      console.log("update data", res);
      this.showToast('success', this.MSG_HEAd_MESSAGE_USER_UPDATED, this.MSG_BODY_MESSAGE_USER_DATA_UPDATED );
      this.getUserDetails();
    }).catch(err=>{
      console.log("error", err);
    })
  }

  updatePassword(){
    //console.log("update password", this.updatePasswordForm.value);
    this.updatePasswordForm.value.email = this.userEmail;
    this.updatePasswordForm.value.password = this.updatePasswordForm.value.con_pass;

    this.user.changePassword(this.updatePasswordForm.value)
      .then(res=>{
        console.log("pass res-", res);
        this.showToast('success', this.MSG_HEAd_MESSAGE_USER_UPDATED,  this.MSG_BODY_MESSAGE_USER_DATA_PASSWORD_UPDATED );
        this.updatePasswordForm.reset();
        this.modalService.dismissAll();
      }).catch(err=>{
        console.log("error", err);
        this.showToast('danger', this.MSG_HEAd_MESSAGE_USER_ERROR, this.MSG_BODY_MESSAGE_USER_DATA_OLD_PASSWORD_UPDATED );
      })    
  }

  setDefaultPic() {
    this.imgurl = this.errUrl;
  }

  


  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? ` ${title}` : '';

    // this.index += 1;
    this.toastrService.show(
      body,
      titleContent,
      config);
  }

}
