import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'ngx-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signinForm: FormGroup;

  constructor(private userService: UserService, public router: Router) { }

  ngOnInit() {

    this.signinForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required])
    });
  }

  login() {
    console.log(this.signinForm.value);
    this.userService.signIn(this.signinForm.value).then(res => {
      console.log('user login ' + res);
      if (res.auth == true) {
        this.router.navigate(['/pages/dashboard']);
      } else {
        window.alert(res.message);
      }
      // this.router.navigate(['/pages/dashboard']);
    });
  }

  createAccount(){
    this.router.navigate(['/pages/user/signup']);
  }


}
