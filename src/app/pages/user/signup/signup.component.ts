import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'
import { UserService } from '../user.service';

@Component({
  selector: 'ngx-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user: any[]
  signupForm: FormGroup;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {

    this.signupForm = new FormGroup({
      'firstName': new FormControl(null, Validators.required),
      'lastName': new FormControl(null, Validators.required),
      'userName': new FormControl(null, Validators.required),
      'about': new FormControl(null),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required])
    });
  }

  onSingup() {
    console.log(this.signupForm.value);
    this.userService.signUp(this.signupForm.value).then(res => {
      console.log('User Registerd !! ' + res)
      this.router.navigate(['/pages/user/signin']);
    });

  }

  login() {
    this.router.navigate(['/pages/user/signin']);
  }



}
