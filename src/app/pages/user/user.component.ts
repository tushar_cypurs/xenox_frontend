import { Component } from '@angular/core';

@Component({
  selector: 'ngx-user-module',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class UserComponent {
}
