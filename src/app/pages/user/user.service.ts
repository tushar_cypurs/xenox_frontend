import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ConfigService } from "../../configuration/config.service";




@Injectable()
export class UserService {

    token: any;
    headers: any;
    editData: any;
    addStockReturnData: any = [];

    constructor(public httpClient: HttpClient, public baseUrl: ConfigService) { }
    apiUrl = this.baseUrl.getBaseUrl();

    getHeader() {
        this.token = sessionStorage.getItem('token');
        let headers: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'x-access-token': this.token
        });
        this.headers = headers;
    }

    

    signUp(user: any[]){
        return this.httpClient.post('http://localhost:5000/reg', user).toPromise()        
    }

    login(user: any[]):Promise<any>{
        return this.httpClient.get('http://localhost:5000/user').toPromise()
        // .then((data:any) => {

        // });
    }

    signIn(user:any[]): Promise<any>{
        return this.httpClient.post('http://localhost:5000/login', user).toPromise()
    }

    login2(user: any[]): Promise<any>{
        return new Promise((resolve,reject)=>{
            
            this.httpClient.get('http://localhost:5000/user').toPromise()
            .then((res)=>{
                //processing
                resolve("Data")
            })
            .catch(err=>{
                reject(err)
            })
            
            
        }) 
        // .then((data:any) => {

        // });
    }

    getEditData(data: any) {
        console.log("service data is", data)
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/auth/getDetails/'+ data, { headers: this.headers }).toPromise()
    }

    getUserImage(data: any) {
        //console.log("service data img is", data)
        this.getHeader();
        return this.httpClient.get(this.apiUrl + '/auth/getDetails/'+data, { headers: this.headers }).toPromise()
    }

    // getImageUrl(data: any) {
    //     return this.apiUrl + '/util/getfile/'+data;
    // }
    getImageUrl() {
        return this.apiUrl + '/util/getfile/';
    }

    updateUserData(data: any) {
        //console.log("service image data", data)
        return this.httpClient.post(this.apiUrl + '/auth/updateUserData', data, { headers: this.headers }).toPromise()
    }

    uploadUserImage(data: any) {
        //console.log("img data in service", data)
        return this.httpClient.post(this.apiUrl + '/util/uploadImage', data).toPromise()
    }

    changePassword(data: any){
        console.log("service data", data)
        return this.httpClient.post(this.apiUrl + '/auth/changePassword', data, { headers: this.headers }).toPromise()
    }

}