export class Products{
    public createdAt;
    public id;
    public isDeleted;
    public price;
    public productDescription;
    public productImage;
    public productName;
    public quantity;
    public sku_id;
    public unit_id;
    public updatedAt;
    constructor(){
        
    }
}