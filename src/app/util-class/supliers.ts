export class Suppliers{
    public id;
    public address;
    public companyId;
    public contactNumber;
    public createdAt;
    public description;
    public email;
    public image;
    public isDeleted;
    public itemId;
    public name;
    public updatedAt;

    constructor(){
        
    }
}