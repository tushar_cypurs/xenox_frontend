/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  baseUrl: "http://localhost/zenox-admin"
  // baseUrl: "https://cypurs.net/zenox-admin"
};
 